extends KinematicBody2D

class_name Player

export var AxisPath : NodePath
export var DefaultGunNode : NodePath

export var BaseSpeed : float
export var MaxHealth : int

func _physics_process(_delta):
	var axisNode = get_node_or_null(AxisPath);
	if (axisNode == null): return;
	
	var slideResult = self.move_and_slide((axisNode.GetAxis() * self.BaseSpeed), Vector2.UP);
	
func _input(event):
	if not (event is InputEventMouseButton):
		return
	
	var gun = get_node_or_null(DefaultGunNode) as Gun;
	if (gun == null): return;
	
	if (event.button_index == BUTTON_LEFT and event.pressed):
		gun.fireBullet(self.global_position, 0.0, 4);
