extends Node

class_name VirtualAxis

export var UpAxisName : String
export var DownAxisName : String
export var LeftAxisName : String
export var RightAxisName : String

var upAxisValue : float
var downAxisValue : float
var leftAxisValue : float
var rightAxisValue : float

func _process(_delta):
	if (Input.is_action_just_pressed(UpAxisName)): upAxisValue = 1.0;
	if (Input.is_action_just_pressed(DownAxisName)): downAxisValue = 1.0;
	if (Input.is_action_just_pressed(LeftAxisName)): leftAxisValue = 1.0;
	if (Input.is_action_just_pressed(RightAxisName)): rightAxisValue = 1.0;
	
	if (Input.is_action_just_released(UpAxisName)): upAxisValue = 0.0;
	if (Input.is_action_just_released(DownAxisName)): downAxisValue = 0.0;
	if (Input.is_action_just_released(LeftAxisName)): leftAxisValue = 0.0;
	if (Input.is_action_just_released(RightAxisName)): rightAxisValue = 0.0;

func GetAxis():
	var axisVector = Vector2.ZERO;
	
	axisVector.x = rightAxisValue - leftAxisValue;
	axisVector.y = downAxisValue - upAxisValue;
	
	return axisVector.normalized();
