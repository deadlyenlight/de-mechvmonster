extends Node

class_name Gun

export var Bullet : PackedScene
export var MaxAmmo : int
export var SpeedModifier : float

var CurrentAmmo : int

func _ready():
	if (MaxAmmo != 0): CurrentAmmo = MaxAmmo;
	

func fireBulletWithDegrees(fromLocation : Vector2, degAngle : float, collisionMask : int):
	return fireBullet(fromLocation, deg2rad(degAngle), collisionMask);

func fireBullet(fromLocation : Vector2, radAngle : float, collisionMask : int):
	if (Bullet == null):
		print("No bullet loaded on " + self.name);
		return null;
	
	var bulletInstance = Bullet.instance();
	get_tree().get_root().add_child(bulletInstance);
	bulletInstance.set_collision_mask(collisionMask);
	var bulletBody = bulletInstance as Bullet;
	if (bulletBody == null):
		bulletInstance.queue_free();
		return null;
	
	bulletBody.global_rotation = radAngle;
	bulletBody.global_position = fromLocation;
	bulletBody.SpeedMod = SpeedModifier;
	return bulletInstance;
