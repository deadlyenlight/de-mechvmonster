%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: RunMask
  m_Mask: 01000000010000000100000000000000000000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Plr_b
    m_Weight: 1
  - m_Path: plr_sk
    m_Weight: 1
  - m_Path: plr_sk/Hip
    m_Weight: 1
  - m_Path: plr_sk/Hip/spine1
    m_Weight: 1
  - m_Path: plr_sk/Hip/spine1/spine2
    m_Weight: 1
  - m_Path: plr_sk/Hip/spine1/spine2/neck
    m_Weight: 1
  - m_Path: plr_sk/Hip/spine1/spine2/neck/head
    m_Weight: 1
  - m_Path: plr_sk/Hip/spine1/spine2/neck/head/head_end
    m_Weight: 1
  - m_Path: plr_sk/Hip/spine1/spine2/l_shoulder
    m_Weight: 1
  - m_Path: plr_sk/Hip/spine1/spine2/l_shoulder/l_arm
    m_Weight: 1
  - m_Path: plr_sk/Hip/spine1/spine2/l_shoulder/l_arm/l_elbow
    m_Weight: 1
  - m_Path: plr_sk/Hip/spine1/spine2/l_shoulder/l_arm/l_elbow/l_hand
    m_Weight: 1
  - m_Path: plr_sk/Hip/spine1/spine2/l_shoulder/l_arm/l_elbow/l_hand/l_hand_end
    m_Weight: 1
  - m_Path: plr_sk/Hip/spine1/spine2/r_shoulder
    m_Weight: 1
  - m_Path: plr_sk/Hip/spine1/spine2/r_shoulder/r_arm
    m_Weight: 1
  - m_Path: plr_sk/Hip/spine1/spine2/r_shoulder/r_arm/r_elbow
    m_Weight: 1
  - m_Path: plr_sk/Hip/spine1/spine2/r_shoulder/r_arm/r_elbow/r_hand
    m_Weight: 1
  - m_Path: plr_sk/Hip/spine1/spine2/r_shoulder/r_arm/r_elbow/r_hand/r_hand_end
    m_Weight: 1
  - m_Path: plr_sk/Hip/l_leg
    m_Weight: 1
  - m_Path: plr_sk/Hip/l_leg/l_knee
    m_Weight: 1
  - m_Path: plr_sk/Hip/l_leg/l_knee/l_foot
    m_Weight: 1
  - m_Path: plr_sk/Hip/l_leg/l_knee/l_foot/l_foot_end
    m_Weight: 1
  - m_Path: plr_sk/Hip/l_leg/l_knee/l_base
    m_Weight: 1
  - m_Path: plr_sk/Hip/l_leg/l_knee/l_base/l_base_end
    m_Weight: 1
  - m_Path: plr_sk/Hip/r_leg
    m_Weight: 1
  - m_Path: plr_sk/Hip/r_leg/r_knee
    m_Weight: 1
  - m_Path: plr_sk/Hip/r_leg/r_knee/r_foot
    m_Weight: 1
  - m_Path: plr_sk/Hip/r_leg/r_knee/r_foot/r_foot_end
    m_Weight: 1
  - m_Path: plr_sk/Hip/r_leg/r_knee/r_base
    m_Weight: 1
  - m_Path: plr_sk/Hip/r_leg/r_knee/r_base/r_base_end
    m_Weight: 1
  - m_Path: plr_sk/l_HANDLE
    m_Weight: 1
  - m_Path: plr_sk/l_HANDLE/l_HANDLE_end
    m_Weight: 1
  - m_Path: plr_sk/l_ATarget
    m_Weight: 1
  - m_Path: plr_sk/l_ATarget/l_ATarget_end
    m_Weight: 1
  - m_Path: plr_sk/l_legTARG
    m_Weight: 1
  - m_Path: plr_sk/l_legTARG/l_legTARG_end
    m_Weight: 1
  - m_Path: plr_sk/r_HANDLE
    m_Weight: 1
  - m_Path: plr_sk/r_HANDLE/r_HANDLE_end
    m_Weight: 1
  - m_Path: plr_sk/r_ATarget
    m_Weight: 1
  - m_Path: plr_sk/r_ATarget/r_ATarget_end
    m_Weight: 1
  - m_Path: plr_sk/r_legTARG
    m_Weight: 1
  - m_Path: plr_sk/r_legTARG/r_legTARG_end
    m_Weight: 1
  - m_Path: plr_sk/l_bIK
    m_Weight: 1
  - m_Path: plr_sk/l_bIK/l_bIK_end
    m_Weight: 1
  - m_Path: plr_sk/r_bIK
    m_Weight: 1
  - m_Path: plr_sk/r_bIK/r_bIK_end
    m_Weight: 1
