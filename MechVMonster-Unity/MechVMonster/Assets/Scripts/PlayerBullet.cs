﻿using UnityEngine;

public class PlayerBullet : MonoBehaviour
{
    [SerializeField] private int _damage = default;
    [SerializeField] private int _pierce = default;

    private void OnCollisionEnter(Collision collision)
    {
        var targetScript = collision.gameObject.GetComponent<IDamagable>();
        if (targetScript != null)
        {
            targetScript.TakeDamage(_damage, _pierce);
        }

        Destroy(gameObject);
    }
}
