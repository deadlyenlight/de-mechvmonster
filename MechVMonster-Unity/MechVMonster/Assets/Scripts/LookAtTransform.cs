﻿using UnityEngine;

public class LookAtTransform : MonoBehaviour
{

    [SerializeField] private Transform _targetTransform = default;
    [SerializeField] private Quaternion _quatOffset = default;

    // Update is called once per frame
    void Update()
    {
        if (_targetTransform != null)
        {
            transform.LookAt(_targetTransform);
            transform.localRotation = new Quaternion(transform.localRotation.x + _quatOffset.x, transform.localRotation.y + _quatOffset.y, transform.localRotation.z + _quatOffset.z, transform.localRotation.w + _quatOffset.w);
        }
    }
}
