﻿using UnityEngine;

public class ObjectiveBase : ScriptableObject, IObjective
{
    public virtual void SubscribeToEvents(LevelControl controller)
    {
        // Override if you want to subscribe.
    }
    
    public virtual ObjectiveCheckResult CheckObjective(ObjectiveData data)
    {
        return ObjectiveCheckResult.NOTCOMPLETE;
    }
}
