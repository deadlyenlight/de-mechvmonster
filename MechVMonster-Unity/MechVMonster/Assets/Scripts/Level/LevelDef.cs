﻿using System;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewLevel", menuName = "MechVMonster/Level/LevelDef")]
public class LevelDef : ScriptableObject
{
    [SerializeField] private List<SpawnEvent> _spawnEvents = default;
    public ICollection<SpawnEvent> SpawnEvents
    {
        get => _spawnEvents;
    }

    [SerializeField] private List<EnvironmentChangeEvent> _environmentEvents = default;
    public ICollection<EnvironmentChangeEvent> EnvironmentEvents
    {
        get => _environmentEvents;
    }

    [SerializeField] private ObjectiveBase _levelObjective = default;
    public ObjectiveBase LevelObjective
    {
        get => _levelObjective;
    }

    [Header("Objective Complete & Secret")]
    [SerializeField] private LevelDef _nextLevel = default;
    public LevelDef NextLevel
    {
        get => _nextLevel;
    }

    [SerializeField] private LevelDef _secretLevel = default;
    public LevelDef SecretLevel
    {
        get => _secretLevel;
    }
}

[Serializable]
public class SpawnAction
{
    public enum SpawnTarget { Player, EvacZone }

    public string TargetSpawner;
    public GameObject TargetPrefab;
    public float SpawnerCooldown;
    public SpawnTarget ActionTarget;
}