﻿using System;
using UnityEngine;
using UnityEngine.Events;

public class SpawnRequestEvent : MonoBehaviour
{
    [Serializable]
    public class SpawnRequestCompleteEvent : UnityEvent<GameObject>
    {

    }

    [SerializeField] private bool _runOnAwake;
    [SerializeField] private GameObject _spawnable;
    [SerializeField] private Transform _spawnTransform;

    public SpawnRequestCompleteEvent SpawnRequestComplete;

    // For this class we need to have someone to report spawns TO
    // We also need a way to control specifically _when_ to request the spawn
    // For instance, we might want to do it on start, or we may want to do it when a Group is killed or when a specific named enemy spawns or dies.

    void Awake()
    {
        if (_runOnAwake) Run();
    }

    public void Run()
    {
        if (_spawnable == null) return;
        var entity = Instantiate(_spawnable, _spawnTransform.position, _spawnTransform.rotation);

        // Might do more here later, for now
        SpawnRequestComplete?.Invoke(entity);
    }
}
