﻿using System.Collections.Generic;
using UnityEngine;

public class LevelEventControl : MonoBehaviour
{
    [SerializeField] private List<Enemy> _activeEnemies = new List<Enemy>();
    public List<Enemy> ActiveEnemies { get => _activeEnemies; }

    [SerializeField] private List<Civilian> _activeCivilians = new List<Civilian>();
    public List<Civilian> ActiveCivilians {get => _activeCivilians; }

    public void AddEnemy (GameObject entity)
    {
        var eEntity = entity.GetComponent<Enemy>();

        if (eEntity == null)
        {
            Debug.LogError($"Could not add an enemy with object {entity.name}, make sure the object has an Enemy Component");
            return;
        }

        _activeEnemies.Add(eEntity);
    }

    public void AddCivilian (GameObject entity)
    {
        var cEntity = entity.GetComponent<Civilian>();

        if (cEntity == null)
        {
            Debug.LogError($"Could not add an civilian with object {entity.name}, make sure the object has an Civilian Component");
            return;
        }

        _activeCivilians.Add(cEntity);
    }
}
