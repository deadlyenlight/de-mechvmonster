﻿using UnityEngine;
using UnityEngine.UI;

public class LevelUI : MonoBehaviour
{
    [SerializeField] private GameOverUIControl _gameOverUIControl = default;
    public GameOverUIControl GameOverUIControl {get => _gameOverUIControl;}

    [SerializeField] private Text _gameOverText = default;
    public Text GameOverText {get => _gameOverText;}


    [SerializeField] private GameObject _gameOverPanel = default;
    public GameObject GameOverPanel {get => _gameOverPanel;}

    [SerializeField] private GameObject _levelCompletePanel = default;
    public GameObject LevelCompletePanel {get => _levelCompletePanel; }
}
