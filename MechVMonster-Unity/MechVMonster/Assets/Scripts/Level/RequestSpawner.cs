﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class RequestSpawner : MonoBehaviour
{
    public Transform TargetTransform = default;

    private List<SpawnRequest> _spawnRequests = new List<SpawnRequest>();
    private float _nextSpawnTime = default;

    public event Action<ISpawnable, string, string> EntitySpawned;

    private void LateUpdate()
    {
        if (_spawnRequests.Count > 0)
        {
            _nextSpawnTime -= Time.deltaTime;

            if (_nextSpawnTime <= 0.0f)
            {
                _nextSpawnTime = SpawnEntity(_spawnRequests[0]);
                _spawnRequests.RemoveAt(0);
            }
        }
    }

    private float SpawnEntity(SpawnRequest requestToHandle)
    {
        if (requestToHandle.Spawnable is MonoBehaviour spawnBehaviour)
        {
            var nextSpawn = Instantiate(spawnBehaviour, transform.position, transform.rotation);

            (nextSpawn as ISpawnable).Name = requestToHandle.EntityName;

            var targetter = nextSpawn.GetComponent<TransformTracker>();

            // TODO: See if we can't find a way to handle this automatically
            if (targetter != null)
            {
                targetter.TargetTransform = TargetTransform;
            }

            var spawnable = nextSpawn as ISpawnable;
            if (spawnable != null)
            {
                spawnable.InitializeSpawn(requestToHandle.SpawnContext);
                EntitySpawned?.Invoke(nextSpawn as ISpawnable, requestToHandle.EntityName, requestToHandle.EntityGroup);
            }
            else
            {
                // TODO: DO SOMETHING??!!
            }
            return requestToHandle.SpawnerCooldown;
        }
        else
        {
            return 0.0f;
        }
    }

    public void AddRequest(SpawnRequest request)
    {
        if (request != null)
        {
            bool countAtZero = _spawnRequests.Count == 0;
            _spawnRequests.Add(request);
            if (countAtZero) { _nextSpawnTime = 0.0f; }
        }
    }
}

public class SpawnRequest
{
    public ISpawnable Spawnable { get; }
    public float SpawnerCooldown { get; }
    public SpawnContext SpawnContext { get; }
    public string EntityName { get; }
    public string EntityGroup {get;}

    // Event for when this dies

    public SpawnRequest(ISpawnable spawnable, SpawnContext context, float cooldown, string name = null, string group = null)
    {
        Spawnable = spawnable;
        SpawnerCooldown = cooldown;
        SpawnContext = context;
        EntityName = name;
        EntityGroup = group;
    }
}
