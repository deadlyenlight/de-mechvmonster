﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class LevelControl : MonoBehaviour
{
    // Switch to ScriptableObject LevelDef system soon.
    public enum LevelState { NotInitialized, PreWave, Running, Failed, Complete, Restarting }

    // We need to track the player
    private Player _activePlayer;
    public Player ActivePlayer {get => _activePlayer; }

    // We need to track all civilians
    private Dictionary<int, Civilian> _activeCivilians;

    // We need to track all named civilians
    private Dictionary<string, Group> _civilianGroups;

    // We need to track all monsters
    private Dictionary<int, Enemy> _activeEnemies;

    // We need to track all named monsters
    private Dictionary<string, Group> _enemyGroups;

    [SerializeField] private PlayerSpawner _playerSpawner = default;
    [SerializeField] private PlayerUIManager _playerUI = default;
    [SerializeField] private List<RequestSpawner> _allSpawners = default;
    [SerializeField] private List<TriggerZone> _allTriggers = default;
    [SerializeField] private EvacZone _evacZone = default;
    public EvacZone EvacZone {get => _evacZone;}

    [SerializeField] private ObjectiveBase _objective = default;
    [SerializeField] private LevelDef _initialLevel = default;

    [Header("UI Links")]
    [SerializeField] private LevelUI _levelUi = default;

    private int _enemiesLoaded = default;
    private int _civiliansLoaded = default;
    private int _enemiesKilled = default;
    private int _civiliansEvacced = default;
    private int _civiliansKilled = default;

    private Dictionary<string, RequestSpawner> _labelledSpawners;
    private LevelState _currentState;

    private LevelDef _currentLevel;
    public LevelDef CurrentLevel
    {
        get => _currentLevel;
        set
        {
            _currentLevel = value;
            if (_currentLevel != null)
            {
                _objective = _currentLevel.LevelObjective;
            }
            _evacZone.Reset();
        }
    }

    public IReadOnlyDictionary<string, RequestSpawner> LabelledSpawners => new ReadOnlyDictionary<string, RequestSpawner>(_labelledSpawners);

    public LevelState CurrentState
    {
        get => _currentState;
        set
        {
            if (_currentState == value) return;

            // Unload current state.
            switch (_currentState)
            {
                case LevelState.NotInitialized:
                    // We start in this state, so we can never enter it.
                    if (LoadControl.GameLoadData != null)
                    {
                        // Load
                        CurrentLevel = LoadControl.GameLoadData.NextLevel;
                        // Destroy Load Data.
                        LoadControl.GameLoadData.Loaded();
                    }
                    else
                    {
                        CurrentLevel = _initialLevel;
                    }
                    break;
                case LevelState.PreWave:
                    // Spawn enemies here.
                    LoadNewLevel();
                    break;
                case LevelState.Running:
                    // May have nothing to do here.
                    break;
                case LevelState.Failed:
                    // Leaving failed state, shut down gameover panel
                    ShowGameOverPanel(false);
                    break;
                case LevelState.Complete:
                    // Close the Save/Continue/Exit Panel
                    break;
                case LevelState.Restarting:
                    RestartLevel();
                    break;
                default:
                    break;
            }

            _currentState = value;

            // Load New State.
            switch (_currentState)
            {
                case LevelState.NotInitialized:
                    // Check for Loading Object, if not load initial level
                    throw new System.Exception("Cannot return to Uninitialized State");
                case LevelState.PreWave:
                    // Spawn player
                    SpawnPlayer();
                    // TODO: Setup wait before wave
                    // For now, switch to running mode.
                    CurrentState = LevelState.Running;
                    break;
                case LevelState.Running:
                    break;
                case LevelState.Failed:
                    ShowGameOverPanel();
                    break;
                case LevelState.Complete:
                    // Load Save/Continue Panel
                    _levelUi.LevelCompletePanel.SetActive(true);
                    _activePlayer.GetComponent<PlayerInput>().enabled = false;
                    break;
                case LevelState.Restarting:
                    CurrentState = LevelState.Running;
                    break;
                default:
                    break;
            }
        }
    }

    #region Level Control Functions
    private Action<LevelControl> _levelStartAction;
    public event Action<LevelControl> LevelStartAction
    {
        add => _levelStartAction += value;
        remove => _levelStartAction -= value;
    }

    private Action<LevelControl, string> _namedEntityDied;
    public event Action<LevelControl, string> NamedEntityDied
    {
        add => _namedEntityDied += value;
        remove => _namedEntityDied -= value;
    }

    private Action<LevelControl, string> _namedCiviliansEvacced;
    public event Action<LevelControl, string> NamedCivilianEvacced
    {
        add => _namedCiviliansEvacced += value;
        remove => _namedCiviliansEvacced -= value;
    }

    private Action<LevelControl, string> _namedEntityHasKilled;
    public event Action<LevelControl, string> NamedEntityHasKilled
    {
        add => _namedEntityHasKilled += value;
        remove => _namedEntityHasKilled -= value;
    }

    private Action<LevelControl, string, float> _namedEntityDamageThreshold;
    public event Action<LevelControl, string, float> NamedEntityDamageThreshold
    {
        add => _namedEntityDamageThreshold += value;
        remove => _namedEntityDamageThreshold -= value;
    }

    private Action<LevelControl, string> _namedGroupEnded;
    public event Action<LevelControl, string> NamedGroupEnded
    {
        add => _namedGroupEnded += value;
        remove => _namedGroupEnded -= value;
    }

    private Action<LevelControl, string> _namedEvent;
    public event Action<LevelControl, string> NamedEvent
    {
        add => _namedEvent += value;
        remove => _namedEvent -= value;
    }
    #endregion

    #region Unity Lifetime
    private void Awake()
    {
        if (_levelUi.GameOverUIControl)
        {
            _levelUi.GameOverUIControl.Restart += Restart_Clicked;
            _levelUi.GameOverUIControl.BackToTitle += BackToTitle_Clicked;
        }

        _labelledSpawners = new Dictionary<string, RequestSpawner>();
        foreach (var spawner in _allSpawners)
        {
            if (_labelledSpawners.ContainsKey(spawner.gameObject.name))
            {
                Debug.LogError($"Cannot add spawner named {spawner.gameObject.name} to the labelled list, there is a spawner with that name already.");
            }
            _labelledSpawners.Add(spawner.gameObject.name, spawner);
            spawner.EntitySpawned += Spawner_EntitySpawned;
        }

        foreach(var trigger in _allTriggers)
        {
            trigger.NamedEventFired += (name) => 
            {
                    _namedEvent?.Invoke(this, name);
            };
        }

        _activeEnemies = new Dictionary<int, Enemy>();
        _enemyGroups = new Dictionary<string, Group>();
        _activeCivilians = new Dictionary<int, Civilian>();
        _civilianGroups = new Dictionary<string, Group>();

        CurrentState = LevelState.PreWave;
    }
    #endregion

    #region Public APIs
    public void NextLevelScreen_Close()
    {
        _activePlayer.GetComponent<PlayerInput>().enabled = true;
        _levelUi.LevelCompletePanel.SetActive(false);
        CurrentState = LevelState.PreWave;
    }

    public void BackToTitle_Clicked(int index)
    {
        SceneManager.LoadScene(index, LoadSceneMode.Single);
    }
    #endregion

    private void SpawnPlayer()
    {
        if (_activePlayer != null) return;
        if (_playerSpawner != null)
        {
            _activePlayer = _playerSpawner.SpawnPlayer();
            _activePlayer.PlayerDied += LevelControl_PlayerDied;
            // To catch any other case of Player destroyed.
            _activePlayer.EntityDestroyed += LevelControl_PlayerDestroyed;
            _playerUI.TargetPlayer = _activePlayer;
        }
    }

    private void Spawner_EntitySpawned(ISpawnable obj, string spawn_name, string group_name)
    {
        if (obj is Enemy enemy)
        {
            _activeEnemies.Add(enemy.gameObject.GetInstanceID(), enemy);
            _enemiesLoaded++;
            enemy.InstanceDies += (id, name) =>
            {
                _enemiesKilled++;

                if (!string.IsNullOrEmpty(name))
                {
                    _namedEntityDied?.Invoke(this, name);
                }

                if (_activeEnemies.ContainsKey(id))
                {
                    _activeEnemies.Remove(id);
                }
                CheckObjective();
            };
            enemy.InstanceThresholdReached += (id, name, threshold) =>
            {
                if (!string.IsNullOrEmpty(name))
                {
                    _namedEntityDamageThreshold?.Invoke(this, name, threshold);
                }
                CheckObjective();
            };
            enemy.InstanceHasKilled += (id, name) =>
            {
                if (!string.IsNullOrEmpty(name))
                {
                    _namedEntityHasKilled?.Invoke(this, name);
                }
                CheckObjective();
            };

            if (group_name != null)
            {
                if (_enemyGroups.ContainsKey(group_name))
                {
                    _enemyGroups[group_name].AddSpawnable(enemy);
                }
                else
                {
                    _enemyGroups.Add(group_name, Group.CreateNewGroup(group_name));
                    _enemyGroups[group_name].GroupEnd.AddListener((name) => {
                        _namedGroupEnded?.Invoke(this, name);
                        _enemyGroups.Remove(name);
                    });
                }
            }
        }
        else if (obj is Civilian civilian)
        {
            _activeCivilians.Add(civilian.gameObject.GetInstanceID(), civilian);
            _civiliansLoaded++;
            civilian.InstanceDestroyed += (id) =>
            {
                if (_activeCivilians.ContainsKey(id))
                {
                    _activeCivilians.Remove(id);
                }
                CheckObjective();
            };
            civilian.InstanceEvacced += (id, name) =>
            {
                _civiliansEvacced++;
                
                if (!string.IsNullOrEmpty(name))
                {
                    _namedCiviliansEvacced?.Invoke(this, name);
                }

                // Record Evac for Level
                _activeCivilians.Remove(id);
                CheckObjective();
            };
            civilian.InstanceKilled += (id, name) =>
            {
                _civiliansKilled++;
                if (!string.IsNullOrEmpty(name))
                {
                    _namedEntityDied?.Invoke(this, name);
                }
                CheckObjective();
            };
        }
    }

    private void Restart_Clicked()
    {
        RestartLevel();
        _levelUi.GameOverText.gameObject.SetActive(false);
        _levelUi.GameOverPanel.gameObject.SetActive(false);
    }

    private void RestartLevel()
    {
        // Clear all entities (but player)
        ClearNonplayers();

        // Clear entity player (will need a new one, this one broke)
        ClearPlayers();

        // Set to 'Before Storm'
        SpawnPlayer();

        // Spawn All (may want to give civilians a head start in the future)
        _levelStartAction?.Invoke(this);

        // Start Level
    }

    private void LoadNewLevel()
    {
        // If Player not active
        if (_activePlayer == null) SpawnPlayer();

        // Clear all entities (but player)
        ClearNonplayers();

        ResetTriggerEvents();

        // Set to 'Before Storm'
        LoadLevelEvents();

        _levelStartAction?.Invoke(this);

        // Spawn All (may want to give civilians a head start in the future)
        // SpawnAll();

        // Start level
    }

    private void ClearNonplayers()
    {
        foreach (var enemy in _activeEnemies)
        {
            if (enemy.Value == null) continue;
            Destroy(enemy.Value.gameObject);
        }

        _enemiesLoaded = 0;
        _enemiesKilled = 0;

        foreach (var civilian in _activeCivilians)
        {
            if (civilian.Value == null) continue;
            Destroy(civilian.Value.gameObject);
        }

        _civiliansLoaded = 0;
        _civiliansKilled = 0;
        _civiliansEvacced = 0;
    }

    private void ClearPlayers()
    {
        if (_activePlayer != null)
        {
            Destroy(_activePlayer.gameObject);
        }
    }

    private void ResetTriggerEvents()
    {
        foreach (var trigger in _allTriggers)
        {
            trigger.ResetTriggerEvents();
        }
    }

#region Level Lifecycle
    private void ClearAllEvents()
    {
        _levelStartAction = delegate{};
        _namedEntityDied = delegate{};
        _namedEntityHasKilled = delegate{};
        _namedEntityDamageThreshold = delegate{};
        _namedEvent = delegate{};
        _namedCiviliansEvacced = delegate{};
    }

    private void LoadLevelEvents()
    {
        if (CurrentLevel == null)
        {
            Debug.LogWarning("Cannot load level events without a level definition loaded");
            return;
        }

        foreach (var spawnEvent in CurrentLevel.SpawnEvents)
        {
            spawnEvent.RegisterEvent(this);
        }

        foreach (var environmentEvent in CurrentLevel.EnvironmentEvents)
        {
            environmentEvent.RegisterEvent(this);
        }

        // Set trigger name rules?
    }
#endregion

    private void LevelControl_PlayerDied()
    {
        // Game Over, no coming back (maybe later make dying okay if the objective is still complete?)
        _activePlayer = null;
        if (CurrentState != LevelState.Complete)
        {
            CurrentState = LevelState.Failed;
        }
    }

    private void LevelControl_PlayerDestroyed()
    {
        // If the player is destroyed, we need to remove it from the connective tissue of the control.
        _activePlayer = null;
    }

    private ObjectiveCheckResult CheckObjective()
    {
        if (_objective == null)
        {
            Debug.LogError("No object set, failing level.");
            CurrentState = LevelState.Failed;
            return ObjectiveCheckResult.FAILED;
        }

        var enemyList = new List<Enemy>();
        foreach (var enemy in _activeEnemies)
        {
            enemyList.Add(enemy.Value);
        }

        var civvieList = new List<Civilian>();
        foreach (var civilian in _activeCivilians)
        {
            civvieList.Add(civilian.Value);
        }

        var data = new ObjectiveData(
            _activePlayer, enemyList, civvieList, 0.0f,
            _civiliansLoaded, _civiliansKilled, _civiliansEvacced,
            _enemiesLoaded, _enemiesKilled);
        var result = _objective.CheckObjective(data);

        switch (result)
        {
            case ObjectiveCheckResult.FAILED:
                CurrentState = LevelState.Failed;
                break;
            case ObjectiveCheckResult.COMPLETE:
                // Load next level
                CurrentLevel = CurrentLevel.NextLevel;
                CurrentState = LevelState.Complete;
                break;
            case ObjectiveCheckResult.SECRET:
                CurrentLevel = CurrentLevel.SecretLevel;
                CurrentState = LevelState.Complete;
                break;
            case ObjectiveCheckResult.NOTCOMPLETE:
            default:
                break;
        }

        return result;
    }

    private void ShowGameOverPanel(bool setActive = true)
    {
        if (_levelUi.GameOverPanel != null) _levelUi.GameOverPanel.SetActive(setActive);
        if (_levelUi.GameOverText != null) _levelUi.GameOverText.gameObject.SetActive(setActive);
        if (_activePlayer != null)
        {
            _activePlayer.GetComponent<PlayerInput>().enabled = !setActive;
            _activePlayer.GetComponent<Collider>().enabled = !setActive;
        }
    }
}
