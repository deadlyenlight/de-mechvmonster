﻿using System.Collections.Generic;

public interface IObjective
{
    ObjectiveCheckResult CheckObjective(ObjectiveData data);
}

public struct ObjectiveData
{
    public Player ActivePlayer;
    public IReadOnlyList<Enemy> ActiveEnemies;
    public IReadOnlyList<Civilian> ActiveCivilians;
    public float TimeSinceLevelStart { get; }

    public int LoadedEnemies { get; }
    public int LoadedCivilians { get; }
    public int EvaccedCivilians { get; }
    public int KilledCivilians { get; }
    public int EnemiesKilled { get; }

    // TODO: May need to clean this up, but this will be the token that allows us to keep data for our IObjectives.
    public ObjectiveData(Player activePlayer, IList<Enemy> enemies, IList<Civilian> civilians, float timeSinceLevelStart,
        int loadedCivilians, int killedCivilians, int evaccedCivilians,
        int loadedEnemies, int enemiesKilled)
    {
        ActivePlayer = activePlayer;
        ActiveEnemies = enemies as IReadOnlyList<Enemy>;
        ActiveCivilians = civilians as IReadOnlyList<Civilian>;
        TimeSinceLevelStart = timeSinceLevelStart;
        LoadedCivilians = loadedCivilians;
        EvaccedCivilians = evaccedCivilians;
        KilledCivilians = killedCivilians;
        LoadedEnemies = loadedEnemies;
        EnemiesKilled = enemiesKilled;
    }
}

public enum ObjectiveCheckResult
{
    NOTCOMPLETE = 0,
    FAILED,
    COMPLETE,
    SECRET
}
