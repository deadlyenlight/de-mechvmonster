﻿using UnityEngine;
using UnityEngine.Events;

public class LoadComponent : MonoBehaviour
{
    private LoadControl loader;

    public UnityEvent SaveDataFound;
    public UnityEvent DataLoaded;

    private void Awake()
    {
        loader = ScriptableObject.CreateInstance<LoadControl>();

        if (loader.SaveExists())
        {
            SaveDataFound?.Invoke();
        }
    }

    public void LoadSave()
    {
        loader.InitializeData();
        DataLoaded?.Invoke();
    }
}
