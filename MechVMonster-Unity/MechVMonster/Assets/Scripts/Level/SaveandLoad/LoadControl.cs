﻿using System.IO;
using System.Text;
using UnityEngine;

public class LoadControl : ScriptableObject
{

    public static LoadControl GameLoadData { get; private set; }

    public LevelDef NextLevel;

    public void InitializeData()
    {
        if (SaveExists())
        {
            // Read next level
            using (FileStream saveFile = File.OpenRead(Path.Combine(Application.persistentDataPath, "saves", "level_control", "data0.txt")))
            {
                byte[] allBytes = new byte[saveFile.Length];
                saveFile.Read(allBytes, 0, allBytes.Length);
                var byteJson = Encoding.UTF8.GetString(allBytes);
                NextLevel = CreateInstance<LevelDef>();
                JsonUtility.FromJsonOverwrite(byteJson, NextLevel);
            }

            GameLoadData = this;
        }
    }

    public void Loaded()
    {
        GameLoadData = null;
        Destroy(this);
    }

    public bool SaveExists()
    {
        return Directory.Exists(Path.Combine(Application.persistentDataPath, "saves"));
    }
}
