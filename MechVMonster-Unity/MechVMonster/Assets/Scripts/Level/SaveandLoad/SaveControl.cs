﻿using System.IO;
using System.Text;
using UnityEngine;

public class SaveControl : MonoBehaviour
{
    public LevelControl LevelControl;

    public void SaveGame()
    {
        if (!SaveExists())
        {
            Directory.CreateDirectory(Path.Combine(Application.persistentDataPath, "saves"));
        }

        // Save the state of the Level Control
        if (!Directory.Exists(Path.Combine(Application.persistentDataPath, "saves", "level_control")))
        {
            Directory.CreateDirectory(Path.Combine(Application.persistentDataPath, "saves", "level_control"));
        }

        WriteSaveData(Path.Combine(Application.persistentDataPath, "saves", "level_control", "data0.txt"), LevelControl.CurrentLevel);

        // Save the state of the Player's Power ups
    }

    private void WriteSaveData<T>(string filePath, T saveGraph)
    {
        if (File.Exists(filePath))
        {
            File.Delete(filePath);
        }
        using (FileStream file = File.Open(filePath, FileMode.OpenOrCreate))
        {
            var json = JsonUtility.ToJson(saveGraph);
            byte[] jsonBytes = Encoding.UTF8.GetBytes(json.ToCharArray());
            file.Write(jsonBytes, 0, jsonBytes.Length);
        }
    }

    public bool SaveExists()
    {
        return Directory.Exists(Path.Combine(Application.persistentDataPath, "saves"));
    }
}
