﻿using UnityEngine;

[System.Serializable]
public class SpawnEvent : LevelEventBase
{
    [Header("Spawn Event Data")]
    [SerializeField] private string EntityName;
    [SerializeField] private string EntityGroupName;
    [SerializeField] private string TargetSpawnerName;
    [SerializeField] private GameObject SpawnPrefab;
    [SerializeField] private float SpawnerCooldown;
    [SerializeField] private SpawnAction.SpawnTarget SpawnTarget;

    protected override void RunEvent(LevelControl controller, params object[] obj)
    {
        if (!controller.LabelledSpawners.ContainsKey(TargetSpawnerName))
        {
            Debug.LogWarning($"Spawner with name [{TargetSpawnerName}] could not be found.");
            return;
        }
        
        var spawner = controller.LabelledSpawners[TargetSpawnerName];
        ISpawnable spawnablePrefab = SpawnPrefab.GetComponent<ISpawnable>();
        if (spawnablePrefab == null)
        {
            Debug.LogError($"Cannot spawn a non-spawnable object, try {typeof(Enemy)} or {typeof(Civilian)} when spawning.");
            return;
        }
        SpawnContext context = new SpawnContext(controller.ActivePlayer.transform, controller.EvacZone.transform, SpawnTarget);
        spawner.AddRequest(new SpawnRequest(spawnablePrefab, context, SpawnerCooldown, EntityName, EntityGroupName));
    }
}
