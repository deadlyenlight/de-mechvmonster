﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class EnvironmentChangeEvent : LevelEventBase
{
    protected override void RunEvent(LevelControl controller, params object[] obj)
    {
        Debug.Log("Environmental Change should happen here!");
    }
}
