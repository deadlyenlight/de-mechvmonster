using System;
using UnityEngine;

[Serializable]
public abstract class LevelEventBase : IDisposable
{
    public enum LevelEventType
    {
        LevelStart,
        NamedEntityDied,
        NamedEntityHasKilled,
        NamedCivilianEvacced,
        NamedEntityDamageThreshold,
        NamedGroupEnded,
        NamedEvent
    }


    [SerializeField] private LevelEventType eventType;
    
    [SerializeField] private string targetName;

    [SerializeField] private float targetThreshold;

    private LevelControl _registeredControl;

    // What sets the event off
    // What the event does
    // Does the result of the event need to publish other events

    // Possible events:
    // // On Level Start
    // // On Death
    // // On Evac
    // // On Kill
    // // On Damage Thresholds (Bosses, typically)

    public void RegisterEvent(LevelControl controller)
    {
        switch (eventType)
        {
            case LevelEventType.LevelStart:
                controller.LevelStartAction += OnLevelStartEvent;
                break;
            case LevelEventType.NamedEntityDied:
                controller.NamedEntityDied += OnNamedEntityDied;
                break;
            case LevelEventType.NamedCivilianEvacced:
                controller.NamedCivilianEvacced += OnNamedCivilianEvacced;
                break;
            case LevelEventType.NamedEntityHasKilled:
                controller.NamedEntityHasKilled += OnNamedEntityHasKilled;
                break;
            case LevelEventType.NamedEntityDamageThreshold:
                controller.NamedEntityDamageThreshold += OnNamedEntityDamageThreshold;
                break;
            case LevelEventType.NamedGroupEnded:
                controller.NamedGroupEnded += OnNamedGroupEnded;
                break;
            case LevelEventType.NamedEvent:
                controller.NamedEvent += OnNamedEvent;
                break;
        }
    }

    protected abstract void RunEvent(LevelControl controller, params object[] obj);

    protected virtual void OnLevelStartEvent(LevelControl controller)
    {
        RunEvent(controller);
    }

    protected virtual void OnNamedEntityDied(LevelControl controller, string name)
    {
        if (name != targetName) return;
        RunEvent(controller, name);
    }

    protected virtual void OnNamedCivilianEvacced(LevelControl controller, string name)
    {
        if (name != targetName) return;
        RunEvent(controller, name);
    }
    protected virtual void OnNamedEntityHasKilled (LevelControl controller, string named)
    {
        if (named != targetName) return;
        RunEvent(controller, named);
    }

    protected virtual void OnNamedEntityDamageThreshold(LevelControl controller, string named, float threshold)
    {
        if (named != targetName) return;
        if (threshold > targetThreshold) return;
        RunEvent(controller, named, threshold);
    }

    protected virtual void OnNamedGroupEnded(LevelControl controller, string group_name)
    {
        if (group_name != targetName) return;
        RunEvent(controller, group_name);
    }
    protected virtual void OnNamedEvent (LevelControl controller, string eventName)
    {
        if (eventName != targetName) return;
        RunEvent(controller);
    }

    public void Dispose()
    {
        if (_registeredControl != null)
        {
            switch (eventType)
            {
                case LevelEventType.LevelStart:
                    _registeredControl.LevelStartAction -= OnLevelStartEvent;
                    break;
                case LevelEventType.NamedEntityDied:
                    _registeredControl.NamedEntityDied -= OnNamedEntityDied;
                    break;
                case LevelEventType.NamedCivilianEvacced:
                    _registeredControl.NamedCivilianEvacced -= OnNamedCivilianEvacced;
                    break;
                case LevelEventType.NamedEntityHasKilled:
                    _registeredControl.NamedEntityHasKilled -= OnNamedEntityHasKilled;
                    break;
                case LevelEventType.NamedEntityDamageThreshold:
                    _registeredControl.NamedEntityDamageThreshold -= OnNamedEntityDamageThreshold;
                    break;
                case LevelEventType.NamedGroupEnded:
                    _registeredControl.NamedGroupEnded -= OnNamedGroupEnded;
                    break;
                case LevelEventType.NamedEvent:
                    _registeredControl.NamedEvent -= OnNamedEvent;
                    break;
            }
        }
    }
}