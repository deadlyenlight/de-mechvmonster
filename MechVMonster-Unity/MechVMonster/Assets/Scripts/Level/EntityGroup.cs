﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EntityGroup : MonoBehaviour
{
    [Serializable]
    public class EntityGroupEndEvent : UnityEvent<GameObject> {}

    [Serializable]
    public class EntityGroupSpawnEvent : UnityEvent {}

    [SerializeField] private bool _spawnOnAwake;
    [SerializeField] private List<GameObject> _groupEntities;

    public EntityGroupEndEvent EndEvent;
    public EntityGroupSpawnEvent GroupSpawn;

    void Awake()
    {
        _groupEntities = new List<GameObject>();

        if (_spawnOnAwake) Spawn();
    }

    public void Spawn()
    {
        GroupSpawn?.Invoke();
    }

    public void AddEntity (GameObject entity)
    {
        if (_groupEntities.Contains(entity)) return;

        var spawnableEntity = entity.GetComponent<SpawnableEventController>();
        if (spawnableEntity != null)
        {
            _groupEntities.Add(entity);
            spawnableEntity.DestroyedEvent.AddListener(RemoveEntity);
        }
    }

    public void RemoveEntity(GameObject entity)
    {
        if (_groupEntities.Contains(entity))
        {
            _groupEntities.Remove(entity);
            if (_groupEntities.Count == 0)
            {
                EndEvent?.Invoke(entity);
            }
        }
    }

}
