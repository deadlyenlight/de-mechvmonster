﻿using UnityEngine;

[CreateAssetMenu(fileName = "NewExterminateObjective", menuName = "MechVMonster/Objective/Exterminate Objective")]
public class ExterminateObjective : ObjectiveBase
{
    [SerializeField] private int _enemyKillCount;

    public override ObjectiveCheckResult CheckObjective(ObjectiveData data)
    {
        if (data.EnemiesKilled >= _enemyKillCount)
        {
            return ObjectiveCheckResult.COMPLETE;
        }

        return ObjectiveCheckResult.NOTCOMPLETE;
    }
}
