﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// TODO: In the future, make this work on a LOGIC gate system (OR, AND, XOR, etc.)
// For now, this is just AND, requiring all objectives be complete.

[CreateAssetMenu(fileName = "NewCompoundObjective", menuName = "MechVMonster/Objective/Compound Objective")]
public class CompoundObjective : ObjectiveBase
{
    [SerializeField] private List<ObjectiveBase> _childObjectives;

    public override void SubscribeToEvents(LevelControl controller)
    {
        foreach (var child in _childObjectives)
        {
            child.SubscribeToEvents(controller);
        }
    }

    public override ObjectiveCheckResult CheckObjective(ObjectiveData data)
    {
        ObjectiveCheckResult result = ObjectiveCheckResult.COMPLETE;

        foreach (var child in _childObjectives)
        {
            var childResult = child.CheckObjective(data);

            if (childResult == ObjectiveCheckResult.FAILED)
            {
                result = ObjectiveCheckResult.FAILED;
                break;
            }

            if (childResult == ObjectiveCheckResult.SECRET)
            {
                result = ObjectiveCheckResult.SECRET;
                break;
            }

            if (childResult == ObjectiveCheckResult.NOTCOMPLETE)
            {
                result = ObjectiveCheckResult.NOTCOMPLETE;
                break;
            }
        }

        return result;
    }


}
