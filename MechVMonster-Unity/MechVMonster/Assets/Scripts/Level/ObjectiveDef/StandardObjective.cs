﻿using UnityEngine;

[CreateAssetMenu(fileName = "NewStandardObjective", menuName = "MechVMonster/Objective/Standard Objective")]
public class StandardObjective : ObjectiveBase
{
    [SerializeField] private int _minimumRequiredRescues = default;

    public override ObjectiveCheckResult CheckObjective(ObjectiveData data)
    {
        if (data.ActivePlayer == null)
        {
            return ObjectiveCheckResult.FAILED;
        }

        if (data.ActiveEnemies.Count == 0)
        {
            if (data.ActiveCivilians.Count == 0)
            {
                if (data.EvaccedCivilians >= _minimumRequiredRescues)
                {
                    return ObjectiveCheckResult.COMPLETE;
                }
                else
                {
                    return ObjectiveCheckResult.FAILED;
                }
            }
        }

        return ObjectiveCheckResult.NOTCOMPLETE;
    }
}
