﻿using UnityEngine;

[CreateAssetMenu(fileName = "NewEventObjective", menuName = "MechVMonster/Objective/Event Objective")]
public class EventCompleteObjective : ObjectiveBase
{
    [SerializeField] private string _eventName;
    private bool _eventHasFired = false;

    public override void SubscribeToEvents(LevelControl controller)
    {
        controller.NamedEvent += (_, name) =>
        {
            if (name == _eventName)
            {
                _eventHasFired = true;
            }
        };
    }

    public override ObjectiveCheckResult CheckObjective(ObjectiveData data) => (_eventHasFired) ? ObjectiveCheckResult.COMPLETE : ObjectiveCheckResult.NOTCOMPLETE;
}
