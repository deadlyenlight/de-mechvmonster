﻿using System;
using UnityEngine;
using UnityEngine.Events;

public class SpawnableEventController : MonoBehaviour
{
    [Serializable] public class SpawnableEventControllerEvent : UnityEvent<GameObject> {}

    public SpawnableEventControllerEvent DestroyedEvent;

    void OnDestroy()
    {
        DestroyedEvent?.Invoke(gameObject);
    }
}
