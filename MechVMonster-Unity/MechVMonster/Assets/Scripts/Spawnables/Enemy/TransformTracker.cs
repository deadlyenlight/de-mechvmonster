﻿using UnityEngine;
using UnityEngine.AI;

public class TransformTracker : MonoBehaviour
{
    private NavMeshAgent _navAgent;
    public NavMeshAgent NavMeshAgent => (_navAgent != null) ? _navAgent : _navAgent = GetComponent<NavMeshAgent>();

    public Transform TargetTransform { get; set; }

    // Update is called once per frame
    void Update()
    {
        if (TargetTransform != null)
        {
            NavMeshAgent.SetDestination(TargetTransform.position);
        }
    }

    public void OnInstanceSpawned(SpawnContext context)
    {
        TargetTransform = context.Target;
    }
}
