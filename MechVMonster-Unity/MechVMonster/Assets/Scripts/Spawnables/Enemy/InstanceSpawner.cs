﻿using UnityEngine;

public class InstanceSpawner : MonoBehaviour
{
    [SerializeField] private Enemy _spawnPrefab = default;
    [SerializeField] private float _respawnSeconds = default;
    public Transform PlayerTransform = default;
    [SerializeField] private bool _automaticRespawn = false;

    bool _instanceSpawned;
    float _timeToSpawn;

    private void Start()
    {
        if (_automaticRespawn)
        {
            SpawnInstance();
        }
    }

    private void Update()
    {
        if (!_automaticRespawn) return;
        if (!_instanceSpawned)
        {
            _timeToSpawn -= Time.deltaTime;
            if (_timeToSpawn <= 0.0f)
            {
                SpawnInstance();
            }
        }
    }

    public Enemy Spawn()
    {
        return SpawnInstance();
    }

    private Enemy SpawnInstance()
    {
        var enemy = Instantiate(_spawnPrefab, transform.position, transform.rotation);
        var tracker = enemy.GetComponent<TransformTracker>();
        if (tracker != null)
        {
            tracker.TargetTransform = PlayerTransform;
        }
        enemy.InstanceDies += Enemy_InstanceDies;
        _instanceSpawned = true;

        return enemy;
    }

    private void Enemy_InstanceDies(int gameObjectInstanceId, string name)
    {
        _instanceSpawned = false;
        _timeToSpawn = _respawnSeconds;
    }
}
