﻿using System;
using UnityEngine;

public class Enemy : MonoBehaviour, IDamagable, ISpawnable
{
    public string Name { get; set; }

    [SerializeField] private int _maxHealth = default;
    [SerializeField] private int _armor = default;
    [SerializeField] private int _contactDamage = default;

    private int _currentHealth;

    public SpawnContextEvent SpawnInitializeEvent = default;

    public event Action<int, string> InstanceDies;
    public event Action<int, string, float> InstanceThresholdReached;
    public event Action<int, string> InstanceHasKilled;

    // Start is called before the first frame update
    void Awake()
    {
        _currentHealth = _maxHealth;
        if (_currentHealth <= 0)
        {
            Debug.LogError($"{gameObject.name} didn't instantiate with health.  You can't live like that.");
            Destroy(gameObject);
        }
    }

    private void OnDestroy()
    {
        if (InstanceDies != null)
        {
            InstanceDies.Invoke(gameObject.GetInstanceID(), Name);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        var playerScript = collision.gameObject.GetComponent<Player>();
        if (playerScript != null)
        {
            playerScript.TakeDamage(_contactDamage, 0);
            var contact = collision.GetContact(0);
            playerScript.AddKnockback(transform.forward * (3000.0f * playerScript.Rigidbody.mass));
            // TODO: Make enemy halt temporarily after striking the player.
        }
    }

    public void TakeDamage(int damage, int pierce)
    {
        if (pierce < _armor) return;

        _currentHealth -= damage;
        if (_currentHealth <= 0)
        {
            Destroy(gameObject);
        }
    }

    public void InitializeSpawn(SpawnContext context)
    {
        if (SpawnInitializeEvent != null)
        {
            SpawnInitializeEvent.Invoke(context);
        }
    }

    public void RegisterThreshold(float threshold)
    {
    }
}
