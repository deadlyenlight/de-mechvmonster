﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Group : MonoBehaviour
{
    public static Group CreateNewGroup(string groupName)
    {
        GameObject container = new GameObject(groupName);
        var group = container.AddComponent<Group>();
        group.GroupName = groupName;
        return group;
    }

    public class GroupOverUnityEvent : UnityEvent<string>
    {
    }

    public string GroupName {get; set;}

    private List<int> spawnables;

    public GroupOverUnityEvent GroupEnd = new GroupOverUnityEvent();

    public void AddSpawnable(Enemy spawnable)
    {
        if (spawnables == null) { spawnables = new List<int>(); }

        if (spawnables.Contains(spawnable.gameObject.GetInstanceID())) return;

        spawnables.Add(spawnable.gameObject.GetInstanceID());
        spawnable.InstanceDies += (id, name) =>
        {
            if (spawnables.Contains(id)) spawnables.Remove(id);

            if (spawnables.Count == 0)
            {
                Destroy(this);
            }
        };
    }

    public void AddSpawnable(Civilian spawnable)
    {
        if (spawnables == null) { spawnables = new List<int>(); }

        if (spawnables.Contains(spawnable.gameObject.GetInstanceID())) return;

        spawnables.Add(spawnable.gameObject.GetInstanceID());
        spawnable.InstanceDestroyed += (id) =>
        {
            if (spawnables.Contains(id)) spawnables.Remove(id);

            if (spawnables.Count == 0)
            {
                Destroy(gameObject);
            }
        };
    }

    void Destroy()
    {
        GroupEnd?.Invoke(GroupName);
    }
}
