﻿using UnityEngine;

[CreateAssetMenu(fileName = "NewAutogun", menuName = "MechVMonster/Guns/New Autogun")]
public class AutoFireGun : Gun
{
    [SerializeField] private GameObject BulletPrefab = default;
    [SerializeField] private float ShotCooldown = default;
    [SerializeField] private AutoGunUiControl AutoGunUiPrefab = default;
    [SerializeField] private AudioClip AutoGunFireClip = default;

    private bool isFiring;
    private float timeToNextBullet;

    public override void HandleGun(GameObject user, Vector3 targetDir, Vector3 targetLocation)
    {
        if (isFiring)
        {
            timeToNextBullet -= Time.deltaTime;

            if (timeToNextBullet <= 0.0f)
            {
                var bullet = Instantiate(BulletPrefab, user.transform.position, user.transform.rotation);
                Debug.DrawRay(bullet.transform.position, bullet.transform.forward * 100.0f, Color.green, 0.2f);
                timeToNextBullet = ShotCooldown;
                OnGunPlaySoundEvent(AutoGunFireClip);
            }
        }
    }

    public override void PullTrigger()
    {
        isFiring = true;
        timeToNextBullet = 0.0f;
    }

    public override void ReleaseTrigger()
    {
        isFiring = false;
    }

    // TODO: Assault Gun UI
    public override void SetupUi(GameObject uiParent)
    {
        if (AutoGunUiPrefab == null)
        {
            Debug.LogError($"No Ui assigned to {name}");
            return;
        }

        Instantiate(AutoGunUiPrefab, uiParent.transform, false);
    }
}
