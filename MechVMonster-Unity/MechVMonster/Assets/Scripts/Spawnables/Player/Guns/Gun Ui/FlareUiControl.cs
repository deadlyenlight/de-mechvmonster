﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FlareUiControl : MonoBehaviour
{
    public List<Image> FlareImages;

    private FlareGun _targetGun = default;
    private bool _initialized = false;

    public void Initialize(FlareGun targetGun)
    {
        if (_initialized) return;

        _targetGun = targetGun;
        _targetGun.GunUiChangedEvent += UpdateFlares;

        UpdateFlares(_targetGun.GetInstanceID());
    }

    private void UpdateFlares(int obj)
    {
        if (_targetGun == null) return;
        for (int flareIndex = 0; flareIndex < FlareImages.Count; ++flareIndex)
        {
            if (flareIndex + 1 > _targetGun.MaxAmmo)
            {
                FlareImages[flareIndex].fillAmount = 0.0f;
            }
            else if (flareIndex == _targetGun.CurrentAmmo)
            {
                FlareImages[flareIndex].fillAmount = (_targetGun.SecondsPerCharge - _targetGun.TimeToNextCharge) / _targetGun.SecondsPerCharge;
            }
            else if (flareIndex > _targetGun.CurrentAmmo)
            {
                FlareImages[flareIndex].fillAmount = 0.0f;
            }
            else
            {
                FlareImages[flareIndex].fillAmount = 1.0f;
            }
        }
    }
}
