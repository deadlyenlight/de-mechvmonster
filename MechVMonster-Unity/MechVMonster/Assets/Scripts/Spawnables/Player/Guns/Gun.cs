﻿using System;
using UnityEngine;

public abstract class Gun : ScriptableObject
{
    public event Action<int> GunUiChangedEvent;
    public event Action<AudioClip> GunPlaySoundEvent;

    public abstract void SetupUi(GameObject uiParent);
    public abstract void HandleGun(GameObject user, Vector3 targetDir, Vector3 targetLocation);
    public abstract void PullTrigger();
    public abstract void ReleaseTrigger();

    protected void OnGunUiChanged()
    {
        GunUiChangedEvent?.Invoke(GetInstanceID());
    }

    protected void OnGunPlaySoundEvent(AudioClip clip)
    {
        GunPlaySoundEvent?.Invoke(clip);
    }
}
