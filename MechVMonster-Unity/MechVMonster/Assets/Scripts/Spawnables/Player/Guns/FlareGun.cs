﻿using UnityEngine;

[CreateAssetMenu(fileName = "New Flare", menuName = "MechVMonster/Guns/New Flare Gun")]
public class FlareGun : Gun
{
    public GameObject FlarePrefab = default;
    public int MaxAmmo = default;
    public float SecondsPerCharge = default;
    public float FireCooldown = default;
    public FlareUiControl FlareUiPrefab = default;

    [SerializeField] private Vector3 _targetLocation;
    private float _timeToNextShot;

    public float TimeToNextCharge { get; private set; }
    public int CurrentAmmo { get; private set; }

    private void Awake()
    {
        CurrentAmmo = MaxAmmo;
        TimeToNextCharge = SecondsPerCharge;
        _timeToNextShot = 0.0f;
    }

    public override void HandleGun(GameObject user, Vector3 targetDir, Vector3 targetLocation)
    {
        _targetLocation = targetLocation;
        _timeToNextShot = (_timeToNextShot > 0.0f) ? _timeToNextShot - Time.deltaTime : 0.0f;
        if (CurrentAmmo < MaxAmmo)
        {
            TimeToNextCharge = (TimeToNextCharge > 0.0f) ? TimeToNextCharge - Time.deltaTime : 0.0f;

            if (TimeToNextCharge <= 0.0f)
            {
                CurrentAmmo++;
                TimeToNextCharge = SecondsPerCharge;
            }
        }
        OnGunUiChanged();
    }

    public override void PullTrigger()
    {
        if (CurrentAmmo > 0 && _timeToNextShot <= 0.0f)
        {
            Instantiate(FlarePrefab, _targetLocation, Quaternion.identity);
            CurrentAmmo--;
            _timeToNextShot = FireCooldown;
        }
    }

    public override void ReleaseTrigger()
    {
    }

    public override void SetupUi(GameObject uiParent)
    {
        if (FlareUiPrefab == null)
        {
            Debug.LogError($"No Ui prefab assigned to {name}");
            return;
        }

        FlareUiControl control = Instantiate(FlareUiPrefab, uiParent.transform, false);
        control.Initialize(this);
    }
}
