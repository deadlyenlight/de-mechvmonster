﻿using System;
using UnityEngine;
using UnityEngine.InputSystem;

public partial class Player
{
    public abstract class PlayerState
    {
        public Player Player { get; }
        public virtual bool AcceptsAltFire { get; } = true;

        public PlayerState(Player player)
        {
            Player = player;
        }

        public abstract void OnStart();
        public abstract void OnEnd();
        public abstract PlayerState Run();
    }

    public class IdlePlayerState : PlayerState
    {
        public IdlePlayerState(Player player) : base(player) { }

        public override void OnEnd() { }

        public override void OnStart()
        {
            // Do nothing.
        }

        public override PlayerState Run()
        {
            return null;
        }
    }

    public class MovingPlayerState : PlayerState
    {
        public MovingPlayerState(Player player) : base(player) { }

        public override void OnEnd()
        {
            Player.MainGun.ReleaseTrigger();
            Player.AltGun.ReleaseTrigger();

            Player.FootStepsAudioSource.Stop();
        }

        public override void OnStart()
        {

        }

        public override PlayerState Run()
        {
            // TODO: Fix aim vectors, for now include the hacky code.
            var aimVector = Player._aimVector;
            if (aimVector == Vector3.zero)
            {
                Vector2 mouseViewport = Player._playCamera.ScreenToViewportPoint(Mouse.current.position.ReadValue()) - new Vector3(0.5f, 0.5f, 0.0f);
                aimVector = new Vector3(mouseViewport.x, 0.0f, mouseViewport.y).normalized;
                Player.transform.rotation = Quaternion.LookRotation(aimVector, Player.transform.up);
            }

            var worldAimPosition = Vector3.zero;

            var ray = Player._playCamera.ScreenPointToRay(Mouse.current.position.ReadValue());
            if (Physics.Raycast(ray, out RaycastHit hit, Player._playCamera.farClipPlane))
            {
                worldAimPosition = hit.point;
            }

            worldAimPosition.y = Player.transform.position.y;

            Player.MainGun.HandleGun(Player.gameObject, aimVector, worldAimPosition);
            Player.AltGun.HandleGun(Player.gameObject, aimVector, worldAimPosition);

            var forwardVec = new Vector3(Player.ForwardVector.x, 0.0f, Player.ForwardVector.z);
            var rightVec = new Vector3(Player.RightVector.x, 0.0f, Player.RightVector.z);

            var desiredMoveDirection = (forwardVec * Player._moveVector.y + rightVec * Player._moveVector.x).normalized;

            Player.Rigidbody.velocity = desiredMoveDirection * Player.Speed;

            if (Player._playerAnimator != null)
            {
                Player._playerAnimator.SetBool("IsMoving", Player.Rigidbody.velocity != Vector3.zero);
            }

            if (Player.Rigidbody.velocity != Vector3.zero)
            {
                if (!Player.FootStepsAudioSource.isPlaying)
                {
                    Player._footStepsAudioSource.loop = true;
                    Player.FootStepsAudioSource.clip = Player.FootStepSound;
                    Player.FootStepsAudioSource.Play(0);
                }
            }
            else
            {
                Player.FootStepsAudioSource.Stop();
            }

            return null;
        }
    }

    public class KnockbackPlayerState : PlayerState
    {
        public float TimeToLive { get; private set; }
        public override bool AcceptsAltFire => false;

        public KnockbackPlayerState(Player player, float stateTimeToLive) : base(player)
        {
            TimeToLive = stateTimeToLive;
        }

        public override void OnEnd()
        {
        }

        public override void OnStart()
        {
        }

        public override PlayerState Run()
        {
            TimeToLive -= Time.deltaTime;
            if (TimeToLive <= 0.0f)
            {
                return new MovingPlayerState(Player);
            }
            return null;
        }
    }

    [Serializable]
    public class PilotPlayerState : PlayerState
    {
        public Mech Mech { get; }

        public PilotPlayerState(Player player, Mech mech) : base(player)
        {
            Mech = mech;
        }

        public override void OnStart()
        {
            // send the player away.
            Player.PlayerInput.enabled = false;
            Player.Agent.enabled = false;
            Player.Collider.enabled = false;
            Player.RenderedModel.SetActive(false);
        }

        public override void OnEnd()
        {
            // bring the player back.
            Player.Agent.enabled = true;
            Player.Collider.enabled = true;
            Player.RenderedModel.SetActive(true);
            var followCam = Player.PlayerCamera.GetComponent<FollowCamera>();
            if (followCam != null) followCam.FollowObject = Player.gameObject;
            Player.PlayerInput.enabled = true;
        }

        public override PlayerState Run()
        {
            if (Mech == null)
            {
                Debug.LogError("MECH HAS DIED SOMEHOW");
                return new MovingPlayerState(Player);
            }
            Player.Rigidbody.position = new Vector3(Mech.transform.position.x, Player.transform.position.y, Mech.transform.position.z);
            return null;
        }
    }
}
