﻿using System;
using UnityEngine;
using UnityEngine.Events;

public class PlayerSpawner : MonoBehaviour
{
    [Serializable]
    public class PlayerInitializedEvent : UnityEvent<Player> {}

    [SerializeField] private bool _runOnAwake;

    [SerializeField] private Player _playerPrefab = default;
    [SerializeField] private FollowCamera _playCam = default;

    public PlayerInitializedEvent PlayerInitialized;

    void Awake()
    {
        if (_runOnAwake) SpawnPlayer();
    }

    public Player SpawnPlayer()
    {
        Player player = Instantiate(_playerPrefab, transform.position, transform.rotation);
        player.PlayerCamera = _playCam.GetComponent<Camera>();
        _playCam.FollowObject = player.gameObject;

        PlayerInitialized?.Invoke(player);

        return player;
    }
}
