﻿using Sirenix.OdinInspector;
using System;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.InputSystem;

public partial class Player : MonoBehaviour, IDamagable
{
    [Header("BASIC STATS")]
    [SerializeField] private Camera _playCamera;
    [SerializeField] private Gun _mainGun = default;
    [SerializeField] private Gun _altGun = default;
    [SerializeField] private float Speed = default;
    [SerializeField] private float IFrameTimeout = default;

    [Header("TRACKABLE STATS")]
    [SerializeField] private int _maxHealth = default;
    [SerializeField] private int _maxShield = default;

    [Header("ANIMATOR CONTROLS")]
    [SerializeField] private Animator _playerAnimator = default;

    [Header("FUNCTIONAL LINKS")]
    [SerializeField] private MechDetector _mechDetector = default;
    [SerializeField] private PlayerInput _playerInput = default;
    [SerializeField] private Collider _collider = default;
    [SerializeField] private GameObject _model = default;
    [SerializeField] private NavMeshAgent _agent = default;

    private Vector2 _moveVector;
    private Vector3 _aimVector;
    private Rigidbody _body;
    private float? _invincibleTime;
    private float? _knockbackTime;
    private Mech _currentMech = default;

    public int MaxHealth
    {
        get => _maxHealth;
        set
        {
            _maxHealth = value;
            CurrentHealth = _maxHealth;
        }
    }

    public int MaxShield
    {
        get => _maxShield;
        set
        {
            _maxShield = value;
            CurrentShield = _maxShield;
        }
    }

    public float? InvincleTime
    {
        get => _invincibleTime;
        private set
        {
            bool hadValue = _invincibleTime.HasValue;
            _invincibleTime = value;
            if (hadValue != _invincibleTime.HasValue)
            {
                PlayerDamaged?.Invoke(value.HasValue);
            }
        }
    }

    private int _currentHealth = default;
    public int CurrentHealth
    {
        get => _currentHealth;
        set
        {
            _currentHealth = Mathf.Clamp(value, 0, MaxHealth);
            PlayerStatsUpdated?.Invoke();
        }
    }

    private int _currentShield = default;
    public int CurrentShield
    {
        get => _currentShield;
        set
        {
            _currentShield = Mathf.Clamp(value, 0, MaxShield);
            PlayerStatsUpdated?.Invoke();
        }
    }

    public Gun MainGun { get; private set; }
    public Gun AltGun { get; private set; }

    public Rigidbody Rigidbody => (_body != null) ? _body : _body = GetComponent<Rigidbody>();

    public PlayerInput PlayerInput => (_playerInput == null) ? _playerInput = GetComponent<PlayerInput>() : _playerInput;

    public Collider Collider => (_collider == null) ? _collider = GetComponent<Collider>() : _collider;

    public NavMeshAgent Agent => (_agent == null) ? _agent = GetComponent<NavMeshAgent>() : _agent;

    public GameObject RenderedModel => _model;

    private GameObject _gunsUiPanel = default;
    public GameObject GunsUiPanel
    {
        get => _gunsUiPanel;
        set
        {
            if (_gunsUiPanel == null)
            {
                MainGun.SetupUi(value);
                AltGun.SetupUi(value);
            }

            _gunsUiPanel = value;
        }
    }

    public Camera PlayerCamera
    {
        get => _playCamera;
        set => _playCamera = value;
    }

    public Vector3 ForwardVector => (_playCamera == null) ? transform.forward : _playCamera.transform.forward;

    public Vector3 RightVector => (_playCamera == null) ? transform.right : _playCamera.transform.right;

    private PlayerState _currentState;
    public PlayerState CurrentState
    {
        get => _currentState;
        set
        {
            if (_currentState != null) _currentState.OnEnd();

            _currentState = value;

            if (_currentState != null) _currentState.OnStart();
        }
    }

    [Header("AUDIO SOURCES")]
    public AudioClip FootStepSound = default;

    private AudioSource _footStepsAudioSource = default;
    [ShowInInspector]
    public AudioSource FootStepsAudioSource
    {
        get => (_footStepsAudioSource != null) ? _footStepsAudioSource : _footStepsAudioSource = GetComponents<AudioSource>()[0];
    }

    private AudioSource _primaryGunAudioSource = default;
    [ShowInInspector]
    public AudioSource PrimaryGunAudioSource
    {
        get => (_primaryGunAudioSource != null) ? _primaryGunAudioSource : _primaryGunAudioSource = GetComponents<AudioSource>()[1];
    }

    public event Action PlayerStatsUpdated;
    public event Action<bool> PlayerDamaged;
    public event Action PlayerDied;
    public event Action EntityDestroyed;

    #region Unity-Methods

    private void Awake()
    {
        CurrentHealth = MaxHealth;
        CurrentShield = MaxShield;

        MainGun = Instantiate(_mainGun);
        AltGun = Instantiate(_altGun);

        if (GunsUiPanel != null)
        {
            MainGun.SetupUi(GunsUiPanel);
            AltGun.SetupUi(GunsUiPanel);
        }

        MainGun.GunPlaySoundEvent += (o) =>
        {
            PrimaryGunAudioSource.PlayOneShot(o);
        };

        CurrentState = new MovingPlayerState(this);
    }

    private void Update()
    {
        if (CurrentState == null)
        {
            CurrentState = new MovingPlayerState(this);
        }

        var nextState = CurrentState.Run();
        if (nextState != null)
        {
            CurrentState = nextState;
        }

        if (InvincleTime.HasValue)
        {
            InvincleTime -= Time.deltaTime;
            if (InvincleTime <= 0.0f)
            {
                InvincleTime = null;
            }
        }
    }

    private void OnDestroy()
    {
        EntityDestroyed?.Invoke();

        if (MainGun != null) Destroy(MainGun);
        if (AltGun != null) Destroy(AltGun);
    }
    #endregion

    #region Input Events
    public void HandleMove(InputAction.CallbackContext obj)
    {
        _moveVector = obj.ReadValue<Vector2>();
    }

    public void HandleMouseAim(InputAction.CallbackContext obj)
    {
        // TODO: Backport Mech Mouse Mechanics.
    }

    public void HandleStickAim(InputAction.CallbackContext obj)
    {
        var stickAim = obj.ReadValue<Vector2>();
        _aimVector = new Vector3(stickAim.x, 0.0f, stickAim.y);
    }

    public void AltFire_Pressed(InputAction.CallbackContext obj)
    {
        if (obj.phase != InputActionPhase.Performed) return;
        if (!CurrentState.AcceptsAltFire) return;
        if (AltGun == null) return;
        AltGun.PullTrigger();
    }

    public void AltFire_Released(InputAction.CallbackContext obj)
    {
        if (obj.phase != InputActionPhase.Performed) return;
        if (AltGun == null) return;
        AltGun.ReleaseTrigger();
    }

    public void Fire_Pressed(InputAction.CallbackContext obj)
    {
        if (obj.phase == InputActionPhase.Performed)
        {
            if (MainGun != null)
            {
                MainGun.PullTrigger();
            }

            if (_playerAnimator != null)
            {
                _playerAnimator.SetBool("IsFiring", true);
            }
        }
    }

    public void Fire_Released(InputAction.CallbackContext obj)
    {
        if (obj.phase == InputActionPhase.Performed)
        {
            if (MainGun != null)
            {
                MainGun.ReleaseTrigger();
            }

            if (_playerAnimator != null)
            {
                _playerAnimator.SetBool("IsFiring", false);
            }
        }
    }

    public void BoardMech_Performed(InputAction.CallbackContext obj)
    {
        if (obj.phase != InputActionPhase.Performed) return;
        // If near mech but not in mech, get out.
        if (_mechDetector != null)
        {
            if (_mechDetector.DetectedMech != null)
            {
                // Shut down input
                if (_mechDetector.DetectedMech.BoardMech(this, BailoutCallback))
                {
                    CurrentState = new PilotPlayerState(this, _mechDetector.DetectedMech);
                    _currentMech = _mechDetector.DetectedMech;
                }
            }
        }
    }

    private void BailoutCallback(Mech mech)
    {
        if (mech != _currentMech) return;

        // Place the player just outside the forward.  Reactivate
        Rigidbody.position = new Vector3(mech.transform.position.x, transform.position.y, transform.position.z) + mech.transform.forward * 3.0f;
        CurrentState = new MovingPlayerState(this);
    }
    #endregion

    public void TakeDamage(int damage, int pierce)
    {
        if (_invincibleTime.HasValue) return;

        if (CurrentShield > 0)
        {
            CurrentShield -= 1;
        }
        else
        {

            CurrentHealth -= damage;

            if (CurrentHealth <= 0)
            {
                PlayerDied?.Invoke();
                Destroy(gameObject);
                return;
            }
        }

        InvincleTime = IFrameTimeout;
    }

    public void AddKnockback(Vector3 force)
    {
        Rigidbody.AddForce(force);
        CurrentState = new KnockbackPlayerState(this, IFrameTimeout / 4.0f);
    }
}
