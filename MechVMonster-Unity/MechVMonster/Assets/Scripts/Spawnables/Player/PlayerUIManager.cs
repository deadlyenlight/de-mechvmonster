﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerUIManager : MonoBehaviour
{

    [SerializeField] private Slider HealthBar = default;
    [SerializeField] private Slider ShieldBar = default;
    [SerializeField] private Slider AmmoBar = default;
    [SerializeField] private Image WeaponImageAvatar = default;
    [SerializeField] private Image PlayerImageAvatar = default;
    [SerializeField] private GameObject _gunUiParent = default;

    [SerializeField] private Player _targetPlayer = default;
    public Player TargetPlayer
    {
        get => _targetPlayer;
        set
        {
            if (_targetPlayer != null)
            {
                _targetPlayer.PlayerStatsUpdated -= UpdateBindings;
            }

            _targetPlayer = value;
            _targetPlayer.PlayerStatsUpdated += UpdateBindings;
            _targetPlayer.GunsUiPanel = _gunUiParent;
            UpdateBindings();
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        if (_targetPlayer != null)
        {
            _targetPlayer.PlayerStatsUpdated += UpdateBindings;
            UpdateBindings();
        }
    }

    private void Update()
    {
        if (_targetPlayer == null) return;
        UpdateBindings();
    }

    private void UpdateBindings()
    {
        HealthBar.minValue = 0.0f;
        HealthBar.maxValue = TargetPlayer.MaxHealth;
        HealthBar.value = TargetPlayer.CurrentHealth;

        ShieldBar.minValue = 0.0f;
        ShieldBar.maxValue = TargetPlayer.MaxShield;
        ShieldBar.value = TargetPlayer.CurrentShield;

        // Update player Ava
        // Update weapon Ava
        // Update ammo count
    }
}
