﻿using System.Collections.Generic;
using UnityEngine;

public class MechDetector : MonoBehaviour
{
    public List<Mech> DetectedMechs { get; private set; }
    public Mech DetectedMech { get => (DetectedMechs.Count > 0) ? DetectedMechs[0] : null; }

    private void Awake()
    {
        DetectedMechs = new List<Mech>();
    }

    private void OnTriggerEnter(Collider other)
    {
        var otherMech = other.GetComponent<Mech>();
        if (otherMech != null)
        {
            if (!DetectedMechs.Contains(otherMech))
            {
                DetectedMechs.Insert(0, otherMech);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        var otherMech = other.GetComponent<Mech>();
        if (otherMech != null && DetectedMechs.Contains(otherMech))
        {
            DetectedMechs.Remove(otherMech);
        }
    }
}
