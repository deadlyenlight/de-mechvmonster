﻿using System;
using UnityEngine;
using UnityEngine.InputSystem;

[RequireComponent(typeof(PlayerInput))]
public partial class Mech : MonoBehaviour, IDamagable
{
    public enum MechState { IDLE, PILOT, DYING }

    [Header("Mech Stats")]
    public int MaxHealth;
    public int ArmorRating;
    public float SpeedRating;

    [Header("Mech Equipment")]
    [SerializeField] private Gun _mechMainGun;
    [SerializeField] private Gun _mechAltGun;

    public MechState State { get; private set; }
    public Vector2 MoveVelocity { get; private set; }
    public Gun MainGun { get; private set; }
    public Gun AltGun { get; private set; }
    public Vector3 AimDirection { get; private set; }
    public float AimMagnitude { get; private set; }
    public Vector3 ForwardVector => (PlayCamera != null) ? PlayCamera.transform.forward : transform.forward;
    public Vector3 RightVector => (PlayCamera != null) ? PlayCamera.transform.right : transform.right;

    [Header("Object Links")]
    [SerializeField] private PlayerInput _playerInput;
    [SerializeField] private Rigidbody _rigidbody;
    [SerializeField] private Animator _mechAnimator;

    public PlayerInput PlayerInput => (_playerInput == null) ? _playerInput = GetComponent<PlayerInput>() : _playerInput;
    public Rigidbody Rigidbody => (_rigidbody == null) ? _rigidbody = GetComponent<Rigidbody>() : _rigidbody;

    private Action<Mech> _bailoutCallback;

    public Camera PlayCamera
    {
        get => PlayerInput.camera;
        set => PlayerInput.camera = value;
    }

    public int CurrentHealth { get; private set; }
    public int CurrentArmorRating { get; private set; }

    private void OnDrawGizmos()
    {
    }

    private void Awake()
    {
        if (_mechMainGun != null) MainGun = Instantiate(_mechMainGun);
        if (_mechAltGun != null) AltGun = Instantiate(_mechAltGun);
    }

    private void Update()
    {
        switch (State)
        {
            case MechState.PILOT:

                if (MainGun != null) MainGun.HandleGun(gameObject, transform.forward, transform.position + transform.forward * AimMagnitude);
                if (AltGun != null) AltGun.HandleGun(gameObject, transform.forward, transform.position + transform.forward * AimMagnitude);

                var forwardVec = new Vector3(ForwardVector.x, 0.0f, ForwardVector.z).normalized;
                var rightVec = new Vector3(RightVector.x, 0.0f, RightVector.z).normalized;

                var desiredMoveDirection = (forwardVec * MoveVelocity.y + rightVec * MoveVelocity.x).normalized;

                Rigidbody.velocity = desiredMoveDirection * SpeedRating;

                if (_mechAnimator != null)
                {
                    // DO ANIMATION.
                }

                break;
        }
    }

    private void OnDestroy()
    {
        if (MainGun != null) Destroy(MainGun);
        if (AltGun != null) Destroy(AltGun);
    }

    public void TakeDamage(int damage, int pierce)
    {
        if (pierce < ArmorRating) return;

        CurrentHealth -= damage;

        if (CurrentHealth <= 0)
        {
            Bail();
            // Die
            State = MechState.DYING;
        }
    }

    public bool BoardMech(Player player, Action<Mech> bailoutCallback)
    {
        if (State != MechState.IDLE) return false;

        // Get data from player (may include upgrades information later.)
        PlayCamera = player.PlayerCamera;
        var followCam = PlayCamera.GetComponent<FollowCamera>();
        if (followCam != null)
        {
            followCam.FollowObject = gameObject;
        }
        _bailoutCallback = bailoutCallback;
        PlayerInput.enabled = true;

        // Set to active
        State = MechState.PILOT;

        return State == MechState.PILOT;
    }

    #region Input Actions
    public void OnMoveCallback(InputAction.CallbackContext context)
    {
        MoveVelocity = context.ReadValue<Vector2>();
    }

    public void OnMainFireCallback(InputAction.CallbackContext context)
    {
        if (context.phase != InputActionPhase.Performed) return;
        if (MainGun == null) return;
        MainGun.PullTrigger();
    }

    public void OnMainFireEndCallback(InputAction.CallbackContext context)
    {
        if (context.phase != InputActionPhase.Performed) return;
        if (MainGun == null) return;
        MainGun.ReleaseTrigger();
    }

    public void OnAltFireCallback(InputAction.CallbackContext context)
    {
        if (context.phase != InputActionPhase.Performed) return;
        if (AltGun == null) return;
        AltGun.PullTrigger();
    }

    public void OnAltFireEndCallback(InputAction.CallbackContext context)
    {
        if (context.phase != InputActionPhase.Performed) return;
        if (AltGun == null) return;
        AltGun.ReleaseTrigger();
    }

    public void OnBailOutCallback(InputAction.CallbackContext context)
    {
        if (context.phase != InputActionPhase.Performed) return;
        if (State != MechState.PILOT) return; // Might need to do more, this suggests a bad state.

        Bail();
        State = MechState.IDLE;
    }

    public void OnMousePositionCallback(InputAction.CallbackContext context)
    {
        if (context.phase != InputActionPhase.Performed) return;
        var mousePosition = context.ReadValue<Vector2>();

        var ray = PlayCamera.ScreenPointToRay(mousePosition);
        if (Physics.Raycast(ray, out RaycastHit hit, PlayCamera.farClipPlane))
        {
            var worldAimPosition = hit.point;
            worldAimPosition.y = transform.position.y;
            transform.LookAt(worldAimPosition);
            AimDirection = transform.forward;
            AimMagnitude = Vector3.Distance(transform.position, worldAimPosition);
        }
    }

    public void OnStickAimCallback(InputAction.CallbackContext context)
    {
        var stickAim = context.ReadValue<Vector2>();
        AimDirection = new Vector3(stickAim.x, 0.0f, stickAim.y);
        AimMagnitude = 3.0f;
    }
    #endregion

    private void Bail()
    {
        PlayerInput.enabled = false;

        _bailoutCallback?.Invoke(this);
        _bailoutCallback = null;
    }
}
