﻿using System;
using UnityEngine;

public class CivilianSpawner : MonoBehaviour
{
    // Make a Wave Spawner that spits out multiple Civilians
    [SerializeField] private Civilian _civilianPrefab = default;
    public Transform SafeZoneTarget = default;
    [SerializeField] private int _waveSize = default;
    [SerializeField] private float _wavePeriod = default;

    private int _currentWaveIndex;
    private float _nextSpawn;
    private bool _waveSpawned;
    private Action<Civilian> _currentCallback;

    private void Awake()
    {
        StartSpawnWave(null);
        _nextSpawn = 0.0f;
    }

    private void Update()
    {
        if (!_waveSpawned)
        {
            _nextSpawn -= Time.deltaTime;
            if (_nextSpawn <= 0.0f)
            {
                SpawnInstance();
                _currentWaveIndex++;
            }

            if (_currentWaveIndex >= _waveSize)
            {
                EndSpawnWave();
            }
        }
    }

    public void StartSpawnWave(Action<Civilian> _spawnedCallback)
    {
        _waveSpawned = false;
        _currentWaveIndex = 0;
        _currentCallback = _spawnedCallback;
    }

    private void EndSpawnWave()
    {
        _waveSpawned = true;
        _currentCallback = null;
    }

    private void SpawnInstance()
    {
        var civvieInstance = Instantiate(_civilianPrefab, transform.position, transform.rotation);
        var safeZoneTracker = civvieInstance.GetComponent<TransformTracker>();
        if (safeZoneTracker != null)
        {
            safeZoneTracker.TargetTransform = SafeZoneTarget;
        }
        _nextSpawn = _wavePeriod;
        _currentCallback?.Invoke(civvieInstance);
    }
}
