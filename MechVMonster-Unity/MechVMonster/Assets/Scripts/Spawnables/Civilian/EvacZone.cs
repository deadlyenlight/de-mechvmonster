﻿using UnityEngine;

public class EvacZone : MonoBehaviour
{
    public int EvaccedCivilians { get; private set; }

    public void Reset()
    {
        EvaccedCivilians = 0;
    }

    private void OnTriggerEnter(Collider other)
    {
        var civilian = other.GetComponent<Civilian>();
        if (civilian != null)
        {
            EvaccedCivilians++;
        }
    }
}
