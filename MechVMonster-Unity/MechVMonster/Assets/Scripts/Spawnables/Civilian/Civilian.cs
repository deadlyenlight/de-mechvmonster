﻿using BehaviorDesigner.Runtime;
using System;
using UnityEngine;

public class Civilian : MonoBehaviour, IDamagable, ISpawnable
{
    public string Name{ get; set;}

    public event Action<int> InstanceDestroyed;
    public event Action<int, string> InstanceEvacced;
    public event Action<int, string> InstanceKilled;
    public event Action<int, string, float> InstanceThresholdReached;

    public SpawnContextEvent SpawnInitializeEvent = default;

    private BehaviorTree _behaviorTree;
    public BehaviorTree BehaviorTree
    {
        get => (_behaviorTree != null) ? _behaviorTree : _behaviorTree = GetComponent<BehaviorTree>();
    }

    public void InitializeSpawn(SpawnContext context)
    {
        BehaviorTree.SetVariableValue("TargetEvac", context.EvacZoneTransform.gameObject);
        BehaviorTree.EnableBehavior();

        if (SpawnInitializeEvent != null)
        {
            SpawnInitializeEvent.Invoke(context);
        }
    }

    public void TakeDamage(int damage, int pierce)
    {
        InstanceKilled?.Invoke(gameObject.GetInstanceID(), name);
        Destroy(gameObject);
    }

    public void Evac()
    {
        // Report successful evac
        InstanceEvacced?.Invoke(gameObject.GetInstanceID(), name);
        Destroy(gameObject);
    }

    private void OnDestroy()
    {
        InstanceDestroyed?.Invoke(gameObject.GetInstanceID());
    }

    public void RegisterThreshold(float threshold)
    {

    }
}
