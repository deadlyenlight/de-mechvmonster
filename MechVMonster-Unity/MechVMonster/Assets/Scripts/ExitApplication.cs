﻿using UnityEngine;

public class ExitApplication : MonoBehaviour
{
    public void ExitApp(int code = 0)
    {
        Application.Quit(code);
    }
}
