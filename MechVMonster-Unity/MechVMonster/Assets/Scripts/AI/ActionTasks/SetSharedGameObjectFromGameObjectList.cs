﻿using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

namespace DeadlyEnlight.Behaviors
{
    [TaskCategory("DEG/Actions")]
    public class IncrementGameObjectListIndex : Action
    {
        public SharedInt TargetIndex;
        public SharedGameObjectList TargetList;

        public override TaskStatus OnUpdate()
        {
            TargetIndex.Value = (TargetIndex.Value + 1) % TargetList.Value.Count;

            return TaskStatus.Success;
        }
    }
}