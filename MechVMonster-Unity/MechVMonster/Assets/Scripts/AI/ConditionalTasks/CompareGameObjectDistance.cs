﻿using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

namespace DeadlyEnlight.Behaviors
{
    [TaskCategory("DEG/Comparison")]
    public class CompareGameObjectDistance : Action
    {

        public SharedGameObject TargetGameObject;
    }
}