﻿using UnityEngine;

public class FollowCamera : MonoBehaviour
{
    [SerializeField]
    private GameObject _followObject = default;
    [SerializeField]
    private float Distance = default;
    [SerializeField]
    private float Angle = default;
    [SerializeField]
    private float Pan = default;

    public GameObject FollowObject
    {
        get => _followObject;
        set => _followObject = value;
    }

    private void LateUpdate()
    {
        if (FollowObject == null) return;

        Vector3 finalPosition = (FollowObject != null) ? FollowObject.transform.position : Vector3.zero;
        transform.rotation = Quaternion.Euler(Pan, Angle, 0.0f);
        transform.position = finalPosition;
        transform.position += transform.forward * -Distance;

        if (FollowObject != null)
        {
            transform.LookAt(FollowObject?.transform ?? transform);
        }
    }
}
