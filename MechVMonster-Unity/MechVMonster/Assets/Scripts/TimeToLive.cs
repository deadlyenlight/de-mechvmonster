﻿using UnityEngine;

public class TimeToLive : MonoBehaviour
{
    [SerializeField] private float TotalTime = default;

    private float timeLeft;

    private void Awake()
    {
        timeLeft = TotalTime;
    }

    private void Update()
    {
        timeLeft -= Time.deltaTime;

        if (timeLeft <= 0.0f)
        {
            Destroy(gameObject);
        }
    }
}
