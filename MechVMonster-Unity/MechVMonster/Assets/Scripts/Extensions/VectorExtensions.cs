﻿using UnityEngine;

public static class VectorExtensions
{
    public static Vector2 Rotate(this Vector2 vector, float degAngle) => Quaternion.Euler(0, 0, degAngle) * vector;

    public static Vector2 RotateByRadians(this Vector2 vector, float radAngle) => vector.Rotate(Mathf.Rad2Deg * radAngle);
}
