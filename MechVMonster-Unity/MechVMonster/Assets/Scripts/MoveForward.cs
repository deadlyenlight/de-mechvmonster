﻿using UnityEngine;

public class MoveForward : MonoBehaviour
{
    [SerializeField] private float Speed = default;

    private Rigidbody _body;
    public Rigidbody Body => (_body != null) ? _body : _body = GetComponent<Rigidbody>();

    private void Update()
    {
        if (Body == null)
        {
            transform.position += (transform.forward * Speed) * Time.deltaTime;
        }
        else
        {
            Body.velocity = (transform.forward * Speed);
        }
    }
}
