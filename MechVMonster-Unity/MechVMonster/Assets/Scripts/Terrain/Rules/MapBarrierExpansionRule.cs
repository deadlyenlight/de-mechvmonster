﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Barrier Rule", menuName = "MechVMonster/Maps/Rules/New Barrier Rule")]
public class MapBarrierExpansionRule : MapRule
{
    public MapCooridorAreaRule CooridorRule;

    public int MinExtraChunks = default;
    public int MaxExtraChunks = default;

    public override List<Vector3Int> HandleRule(MapRuleData context)
    {
        var gridXExtents = (context.Width + 2) / 2;
        var gridZExtents = (context.Height + 2) / 2;
        var minExtents = Mathf.Min(gridXExtents, gridZExtents);

        // Find the edges of the map
        var edgeChunks = new List<Vector3Int>();
        for (int count = -(minExtents - 1); count < gridXExtents || count < gridZExtents; ++count)
        {
            if (Mathf.Abs(count) <= gridXExtents)
            {
                edgeChunks.Add(new Vector3Int(count, 0, -gridZExtents));
                edgeChunks.Add(new Vector3Int(count, 0, gridZExtents));
            }

            if (Mathf.Abs(count) <= gridZExtents)
            {
                edgeChunks.Add(new Vector3Int(-gridXExtents, 0, count));
                edgeChunks.Add(new Vector3Int(gridXExtents, 0, count));
            }
        }

        AddEdgeChunks(context, edgeChunks);

        if (CooridorRule != null)
        {
            CooridorRule.HandleRule(context, new Vector3Int(-gridXExtents, 0, -gridZExtents));
            CooridorRule.HandleRule(context, new Vector3Int(gridXExtents, 0, -gridZExtents));
            CooridorRule.HandleRule(context, new Vector3Int(gridXExtents, 0, gridZExtents));
            CooridorRule.HandleRule(context, new Vector3Int(-gridXExtents, 0, gridZExtents));
        }

        // Make sure for the corners you handle it in both directions
        // Since we're operating with the assumption we're part of the deform stage,
        // we should not return new border chunks.
        return new List<Vector3Int>();
    }

    public override List<Vector3Int> Undo(Dictionary<Vector3Int, GameObject> objectMap)
    {
        return null;
    }

    private void AddEdgeChunks(MapRuleData context, List<Vector3Int> edgeChunks)
    {
        var gridXExtents = (context.Width + 2) / 2;

        foreach (var chunk in edgeChunks)
        {
            int chunksToAdd = Random.Range(MinExtraChunks, MaxExtraChunks + 1);
            var chunkDirection = (Mathf.Abs(chunk.x) == gridXExtents) ?
                new Vector3Int(System.Math.Sign(chunk.x), 0, 0) :
                new Vector3Int(0, 0, System.Math.Sign(chunk.z));

            for (int count = 0; count < chunksToAdd; ++count)
            {
                var currentChunk = chunk + (chunkDirection * count);

                if (context.objectMap.ContainsKey(currentChunk)) continue;

                MapChunk(context, currentChunk, null);
            }
        }
    }
}
