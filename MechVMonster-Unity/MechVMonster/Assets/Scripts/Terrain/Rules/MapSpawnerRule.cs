﻿using Sirenix.OdinInspector;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Spawner Rule", menuName = "MechVMonster/Maps/Rules/New Spawner Rule")]
public class MapSpawnerRule : MapRule
{

    public List<TerrainCell> SpawnerOptions;
    public MapCooridorAreaRule CooridorRule;
    public RepeatableMapRuleOption ShouldRepeat;
    [EnableIf(nameof(ShouldRepeat), RepeatableMapRuleOption.RepeatNumber)] public int RepeatTimes;


    public override List<Vector3Int> HandleRule(MapRuleData context)
    {
        int timesRun = 0;
        while (ShouldContinue(context, timesRun) && context.borderEntities.Count > 0)
        {
            var nextSpawnPoint = context.borderEntities[Random.Range(0, context.borderEntities.Count)];
            if (context.objectMap.ContainsKey(nextSpawnPoint))
            {
                context.borderEntities.Remove(nextSpawnPoint);
                continue;
            }

            bool success = MapChunk(context, nextSpawnPoint, SpawnerOptions[Random.Range(0, SpawnerOptions.Count)]);

            timesRun++;

            if (CooridorRule == null && success) continue;

            var newBorderChunks = CooridorRule.HandleRule(context, nextSpawnPoint);

            // Fix borders
            UpdateBorder(context, newBorderChunks);
        }

        return new List<Vector3Int>();
    }

    public override List<Vector3Int> Undo(Dictionary<Vector3Int, GameObject> objectMap)
    {
        return null;
    }

    private bool ShouldContinue(MapRuleData context, int timesRun)
    {
        switch (ShouldRepeat)
        {
            case RepeatableMapRuleOption.NoRepeat:
                return timesRun == 0;
            case RepeatableMapRuleOption.RepeatNumber:
                return timesRun < RepeatTimes;
            case RepeatableMapRuleOption.FillRemainingArea:
                return context.borderEntities.Count != 0;
            default:
                // We have invalid data and should not run.
                Debug.LogError($"Invalid [ShouldRepeat] option {ShouldRepeat} on {this.name}, please check your settings.");
                return false;
        }
    }
}
