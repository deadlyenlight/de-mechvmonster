﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Sector Rule", menuName = "MechVMonster/Maps/Rules/New Sector Rule")]
public class SectorMapRule : MapRule
{

    [Header("Sectoring Rules")]
    public int SectorCount = 2;
    public int ExitSectorRadius = 3;
    public int SectorSpawnerCountMin = 1;
    public int SectorSpawnerCountMax = 3;

    [Header("Sector Properties")]
    public Color EvacSectorColor = Color.red + Color.blue;
    public Color EntrySectorColor = Color.blue;
    public List<Color> OtherSectorColors;

    public override List<Vector3Int> HandleRule(MapRuleData context)
    {
        MaterialPropertyBlock debugProp = new MaterialPropertyBlock();
        debugProp.SetColor("_Color", Color.green);

        // First, Create the Exit Sector (N Sector)
        var evacCell = context.objectMap.EvacZoneKeys[0];
        // Create a list of keys to 'sector' based on our radius.
        var mpb = new MaterialPropertyBlock();
        mpb.SetColor("_Color", EvacSectorColor);
        var evacSector = new Sector(mpb);
        for (int xAmp = -ExitSectorRadius; xAmp <= ExitSectorRadius; ++xAmp)
        {
            for (int zAmp = -ExitSectorRadius; zAmp <= ExitSectorRadius; ++zAmp)
            {
                var currentCellId = new Vector3Int(xAmp, 0, zAmp);
                if (!context.objectMap.UnsectoredKeys.Contains(currentCellId)) continue;
                context.objectMap[currentCellId].TerrainRenderer.SetPropertyBlock(debugProp);
                if (Vector3.Distance(evacCell, currentCellId) <= ExitSectorRadius)
                {
                    evacSector.AddCell(new Vector3Int(xAmp, 0, zAmp), context.objectMap[currentCellId]);
                    context.objectMap.SectorKey(currentCellId);
                }
            }
        }

        // Set the Entrance Sector (Sector 1)



        // Finally, fill in the missing sectors.


        return null;
    }

    public override List<Vector3Int> Undo(Dictionary<Vector3Int, GameObject> objectMap)
    {
        return null;
    }



}
