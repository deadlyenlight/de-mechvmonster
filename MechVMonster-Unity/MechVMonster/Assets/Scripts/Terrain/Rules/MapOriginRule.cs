﻿using Sirenix.OdinInspector;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Origin Rule", menuName = "MechVMonster/Maps/Rules/New Origin Rule")]
public class MapOriginRule : MapRule
{
    public List<TerrainCell> RuleSegmentOptions;
    public MapCooridorAreaRule CooridorAreaRule;
    public bool StaticStartLocation;
    [EnableIf("StaticStartLocation")]
    public Vector3Int StartLocation;

    public override List<Vector3Int> HandleRule(MapRuleData context)
    {
        if (!StaticStartLocation)
        {
            StartLocation = new Vector3Int(Random.Range(-context.Width / 4, context.Width / 4 + 1), 0, Random.Range(-context.Height / 4, context.Height / 4 + 1));
        }

        var prefab = RuleSegmentOptions[Random.Range(0, RuleSegmentOptions.Count)];
        if (prefab == null) return null;

        context.objectMap.Add(StartLocation, Instantiate(prefab, context.mapGrid.CellToWorld(StartLocation), prefab.transform.rotation));
        if (CooridorAreaRule == null)
        {
            // get the border chunks created by the origin point.
            var finalBorderChunks = new List<Vector3Int>
            {
                StartLocation + Vector3Int.left,
                StartLocation + Vector3Int.right,
                StartLocation + new Vector3Int(0, 0, -1),
                StartLocation + new Vector3Int(0, 0, 1)
            };
            return finalBorderChunks;
        }
        return CooridorAreaRule.HandleRule(context, StartLocation);
    }

    public override List<Vector3Int> Undo(Dictionary<Vector3Int, GameObject> objectMap)
    {
        return null;
    }
}
