﻿using Sirenix.OdinInspector;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Cooridor Rule", menuName = "MechVMonster/Maps/Rules/New Cooridor Rule")]
public class MapCooridorAreaRule : MapRule
{
    public List<TerrainCell> CooridorOptions;
    public uint CooridorArea;
    public bool AllowOutOfBounds;

    [ShowInInspector, DisableInNonPrefabs, DisableInPrefabAssets]
    public Vector3Int OriginPoint { get; set; }

    public List<Vector3Int> HandleRule(MapRuleData context, Vector3Int originPoint)
    {
        OriginPoint = originPoint;
        return HandleRule(context);
    }

    public override List<Vector3Int> HandleRule(MapRuleData context)
    {
        return CreateCooridorRing(OriginPoint, CooridorArea, context);
    }

    public List<Vector3Int> Undo(Dictionary<Vector3Int, GameObject> objectMap, Vector3Int originPoint)
    {
        OriginPoint = originPoint;
        return Undo(objectMap);
    }

    public override List<Vector3Int> Undo(Dictionary<Vector3Int, GameObject> objectMap)
    {
        return null;
    }

    private List<Vector3Int> CreateCooridorRing(Vector3Int originCell, uint ringRadius, MapRuleData context)
    {
        var newBorderChunks = new List<Vector3Int>();
        var points = new List<Vector3Int>
        {
            new Vector3Int( 1, 0, 0),
            new Vector3Int( 0, 0, 1),
            new Vector3Int(-1, 0, 0),
            new Vector3Int( 0, 0,-1)
        };

        if (!AllowOutOfBounds && (Mathf.Abs(originCell.x) > context.Width / 2 || Mathf.Abs(originCell.z) > context.Height / 2))
        {
            return newBorderChunks;
        }

        for (int currentPoint = 0; currentPoint < points.Count; ++currentPoint)
        {
            var currentGridPoint = points[currentPoint] + originCell;

            if (ringRadius == 0)
            {
                if (context.objectMap.ContainsKey(currentGridPoint)) continue;
                newBorderChunks.Add(currentGridPoint);
                continue;
            }

            if (!context.objectMap.ContainsKey(currentGridPoint))
            {
                MapChunk(context, currentGridPoint, CooridorOptions[Random.Range(0, CooridorOptions.Count)]);
            }

            if (ringRadius > 0)
            {
                var otherBorderChunks = CreateCooridorRing(currentGridPoint, ringRadius - 1, context);

                foreach (var chunk in otherBorderChunks)
                {
                    if (newBorderChunks.Contains(chunk)) continue;
                    newBorderChunks.Add(chunk);
                }
            }
        }

        return newBorderChunks;
    }
}
