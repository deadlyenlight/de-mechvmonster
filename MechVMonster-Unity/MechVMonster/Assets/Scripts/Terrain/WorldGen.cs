﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldGen : MonoBehaviour
{
    public class WorldMap : IDictionary<Vector3Int, TerrainCell>
    {
        private Dictionary<Vector3Int, TerrainCell> _mainDictionary;

        private List<Vector3Int> _unsectoredKeys;
        private List<Vector3Int> _spawnerKeys;
        private List<Vector3Int> _evacZoneKeys;

        public TerrainCell this[Vector3Int key]
        {
            get => _mainDictionary[key];
            set
            {
                // Check if the new value needs to be recorded for SPAWNERKEYS and EVACZONEKEYS
                _mainDictionary[key] = value;
            }
        }

        public WorldMap()
        {
            _mainDictionary = new Dictionary<Vector3Int, TerrainCell>();
            _spawnerKeys = new List<Vector3Int>();
            _evacZoneKeys = new List<Vector3Int>();
            _unsectoredKeys = new List<Vector3Int>();
        }

        public IList<Vector3Int> SpawnerKeys => _spawnerKeys.AsReadOnly();

        public IList<Vector3Int> EvacZoneKeys => _evacZoneKeys.AsReadOnly();

        public IList<Vector3Int> UnsectoredKeys => _unsectoredKeys.AsReadOnly();

        public ICollection<Vector3Int> Keys => _mainDictionary.Keys;

        public ICollection<TerrainCell> Values => _mainDictionary.Values;

        public int Count => _mainDictionary.Count;

        public bool IsReadOnly => _mainDictionary is IReadOnlyDictionary<Vector3Int, TerrainCell>;

        public void Add(Vector3Int key, TerrainCell value)
        {
            if (_mainDictionary.ContainsKey(key))
            {
                Debug.LogError($"The World Map already has a tile for {key}, dropping this tile");
                return;
            }

            if (value.AllSpawners.Count != 0) { _spawnerKeys.Add(key); }
            if (value.AllEvacZones.Count != 0) { _evacZoneKeys.Add(key); }

            _unsectoredKeys.Add(key);

            _mainDictionary.Add(key, value);
        }

        public void Add(KeyValuePair<Vector3Int, TerrainCell> item)
        {
            Add(item.Key, item.Value);
        }

        public void Clear()
        {
            _spawnerKeys.Clear();
            _evacZoneKeys.Clear();
            _mainDictionary.Clear();
        }

        public bool Contains(KeyValuePair<Vector3Int, TerrainCell> item)
        {
            return ContainsKey(item.Key) && _mainDictionary[item.Key].GetInstanceID() == item.Value.GetInstanceID();
        }

        public bool ContainsKey(Vector3Int key)
        {
            return _mainDictionary.ContainsKey(key);
        }

        public void CopyTo(KeyValuePair<Vector3Int, TerrainCell>[] array, int arrayIndex)
        {
            /* TODO: Uhhh...? */
        }

        public IEnumerator<KeyValuePair<Vector3Int, TerrainCell>> GetEnumerator()
        {
            return _mainDictionary.GetEnumerator();
        }

        public bool Remove(Vector3Int key)
        {
            if (!_mainDictionary.ContainsKey(key))
            {
                Debug.LogError($"No such key {key} exists in the worldmap");
                return false;
            }

            if (_spawnerKeys.Contains(key)) _spawnerKeys.Remove(key);
            if (_evacZoneKeys.Contains(key)) _evacZoneKeys.Remove(key);

            _mainDictionary.Remove(key);

            return true;
        }

        public bool SectorKey(Vector3Int key)
        {
            if (!_unsectoredKeys.Contains(key))
            {
                return false;
            }

            return _unsectoredKeys.Remove(key);
        }

        public bool Remove(KeyValuePair<Vector3Int, TerrainCell> item)
        {
            return Remove(item.Key);
        }

        public bool TryGetValue(Vector3Int key, out TerrainCell value)
        {
            if (_mainDictionary.ContainsKey(key))
            {
                value = _mainDictionary[key];
                return true;
            }

            value = null;
            return false;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _mainDictionary.GetEnumerator();
        }
    }

    [Header("Map Definitions")]
    public int Height = 12;
    public int Width = 12;

    [Header("Map Rules")]
    public List<MapRule> RulesToUse = default;

    [Header("Tile Options")]
    public List<TerrainCell> BarrierOptions = default;

    private Grid LevelGrid;

    [ShowInInspector] [ReadOnly] Vector3Int LevelGridExtents;
    [ShowInInspector] [ReadOnly] List<Vector3Int> LevelBorderPoints;
    [ShowInInspector] [ReadOnly] Dictionary<Vector3Int, TerrainCell> MappedObjects;
    [ShowInInspector] [ReadOnly] Dictionary<Sector, List<Vector3Int>> SectoredObjects;
    [ShowInInspector] [ReadOnly] WorldMap MappedWorld;

    // Start is called before the first frame update
    void Awake()
    {
        LevelGrid = gameObject.AddComponent<Grid>();
        MappedObjects = new Dictionary<Vector3Int, TerrainCell>();
        LevelBorderPoints = new List<Vector3Int>();
        MappedWorld = new WorldMap();
        SectoredObjects = new Dictionary<Sector, List<Vector3Int>>();

        // Generate the World based on the following Criteria:

        // Distances handled in a Radial Fashion

        // 1. Pick an Initial Spawn Prefab at Random
        MapRule.MapRuleData genContext = new MapRule.MapRuleData
        {
            objectMap = MappedWorld,
            mapGrid = LevelGrid,
            Width = Width,
            Height = Height,
            borderEntities = LevelBorderPoints
        };

        // Run each rule in turn until they are finished.
        foreach (var rule in RulesToUse)
        {
            var newBorderChunks = rule.HandleRule(genContext);
            LevelBorderPoints = genContext.borderEntities; // In case anything is discovered that needs to be back ported.
            if (newBorderChunks?.Count > 0)
            {
                UpdateBorderChunks(newBorderChunks);
                genContext.borderEntities = LevelBorderPoints;
            }
        }
    }

    private void UpdateBorderChunks(List<Vector3Int> newBorderChunks)
    {
        if (LevelBorderPoints.Count == 0) { LevelBorderPoints = newBorderChunks; return; }

        var pointsToRemove = new List<Vector3Int>();
        foreach (var chunk in LevelBorderPoints)
        {
            if (MappedObjects.ContainsKey(chunk))
            {
                pointsToRemove.Add(chunk);
            }
        }

        foreach (var chunk in pointsToRemove)
        {
            LevelBorderPoints.Remove(chunk);
        }

        LevelBorderPoints.AddRange(newBorderChunks);
    }

    private bool MapGameObject(TerrainCell prefab, Vector3Int gridPoint)
    {
        if (Mathf.Abs(gridPoint.x) > Width / 2 || Mathf.Abs(gridPoint.z) > Height / 2)
        {
            // Map border chunk.
            var borderPrefab = BarrierOptions[Random.Range(0, BarrierOptions.Count)];
            MappedObjects.Add(gridPoint, Instantiate<TerrainCell>(borderPrefab, LevelGrid.CellToWorld(gridPoint), prefab.transform.rotation));

            // Return false.
            return false;
        }

        MappedObjects.Add(gridPoint, Instantiate(prefab, LevelGrid.CellToWorld(gridPoint), prefab.transform.rotation));
        return true;
    }
}
