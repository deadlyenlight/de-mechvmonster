﻿using UnityEngine;

public class Destructible : MonoBehaviour, IDamagable
{
    public int MaxHealth = default;
    [SerializeField] private int RequiredPierce = default;

    public int CurrentHealth { get; private set; }

    public void TakeDamage(int damage, int pierce)
    {
        if (pierce < RequiredPierce) return;

        CurrentHealth -= damage;

        if (CurrentHealth <= 0)
        {
            Destroy(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        CurrentHealth = MaxHealth;
    }

    // Update is called once per frame
    void Update()
    {

    }
}
