﻿using System;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class EnemyTarget : MonoBehaviour, ITriggerTarget
{
    public event Action<Collider> TargetTriggerEntered;
    public event Action<Collider> TargetTriggerExited;
    public event Action<Collider> TargetTriggerStayed;

    private void OnTriggerEnter(Collider other)
    {
        if (TargetTriggerEntered == null) return;
        if (other.GetComponent<Enemy>() != null)
        {
            TargetTriggerEntered.Invoke(other);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (TargetTriggerExited == null) return;
        if (other.GetComponent<Enemy>() != null)
        {
            TargetTriggerExited.Invoke(other);
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (TargetTriggerStayed == null) return;
        if (other.GetComponent<Enemy>() != null)
        {
            TargetTriggerStayed.Invoke(other);
        }
    }
}
