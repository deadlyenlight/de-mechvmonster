﻿using System;
using UnityEngine;

public interface ITriggerTarget
{
    event Action<Collider> TargetTriggerEntered;
    event Action<Collider> TargetTriggerExited;
    event Action<Collider> TargetTriggerStayed;
}
