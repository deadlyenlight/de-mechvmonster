﻿using System;
using UnityEngine;

public class AnyTarget : MonoBehaviour, ITriggerTarget
{
    public event Action<Collider> TargetTriggerEntered;
    public event Action<Collider> TargetTriggerExited;
    public event Action<Collider> TargetTriggerStayed;

    private void OnTriggerEnter(Collider other)
    {
        if (TargetTriggerEntered == null) return;
        TargetTriggerEntered(other);
    }

    private void OnTriggerExit(Collider other)
    {
        if (TargetTriggerExited == null) return;
        TargetTriggerExited(other);
    }

    private void OnTriggerStay(Collider other)
    {
        if (TargetTriggerStayed == null) return;
        TargetTriggerStayed(other);
    }
}
