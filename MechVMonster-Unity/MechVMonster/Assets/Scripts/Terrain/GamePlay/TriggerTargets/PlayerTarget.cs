﻿using System;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class PlayerTarget : MonoBehaviour, ITriggerTarget
{
    public event Action<Collider> TargetTriggerEntered;
    public event Action<Collider> TargetTriggerExited;
    public event Action<Collider> TargetTriggerStayed;

    private void OnTriggerEnter(Collider other)
    {
        if (TargetTriggerEntered == null) return;
        if (other.GetComponent<Player>() != null || other.GetComponent<Mech>() != null)
        {
            TargetTriggerEntered.Invoke(other);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (TargetTriggerExited == null) return;
        if (other.GetComponent<Player>() != null || other.GetComponent<Mech>() != null)
        {
            TargetTriggerExited.Invoke(other);
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (TargetTriggerStayed == null) return;
        if (other.GetComponent<Player>() != null || other.GetComponent<Mech>() != null)
        {
            TargetTriggerStayed.Invoke(other);
        }
    }
}
