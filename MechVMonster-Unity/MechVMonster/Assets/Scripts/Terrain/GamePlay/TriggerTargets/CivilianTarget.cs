﻿using System;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class CivilianTarget : MonoBehaviour, ITriggerTarget
{
    public event Action<Collider> TargetTriggerEntered;
    public event Action<Collider> TargetTriggerExited;
    public event Action<Collider> TargetTriggerStayed;

    private Collider _mainCollider;
    public Collider MainCollider
    {
        get => (_mainCollider != null) ? _mainCollider : _mainCollider = GetComponent<Collider>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (TargetTriggerEntered == null) return;
        if (other.GetComponent<Civilian>() != null)
        {
            TargetTriggerEntered.Invoke(other);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (TargetTriggerExited == null) return;
        if (other.GetComponent<Civilian>() != null)
        {
            TargetTriggerExited.Invoke(other);
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (TargetTriggerStayed == null) return;
        if (other.GetComponent<Civilian>() != null)
        {
            TargetTriggerStayed.Invoke(other);
        }
    }
}
