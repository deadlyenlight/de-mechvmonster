﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class TriggerZone : MonoBehaviour
{
    public enum TargetType { Any, Player, Enemy, Civilian }

    [Serializable]
    public class SpawnEvent
    {
        public RequestSpawner spawner;
        public GameObject subject;
    }

    public TargetType Target = TargetType.Any;
    public int MaxRunTimes = default;
    public string eventName;

    private int runTimes = 0;

    public event Action<string> NamedEventFired;

    public void ResetTriggerEvents()
    {
        runTimes = 0;
    }

    private void Awake()
    {
        ITriggerTarget target = null;
        switch (Target)
        {
            case TargetType.Player:
                target = gameObject.AddComponent<PlayerTarget>();
                break;
            case TargetType.Enemy:
                target = gameObject.AddComponent<EnemyTarget>();
                break;
            case TargetType.Civilian:
                target = gameObject.AddComponent<CivilianTarget>();
                break;
            case TargetType.Any:
            default:
                target = gameObject.AddComponent<AnyTarget>();
                break;
        }
        target.TargetTriggerEntered += TriggerRun;
    }

    void TriggerRun(Collider other)
    {
        if (runTimes >= MaxRunTimes) return;
        runTimes++;
        Debug.Log($"Running {eventName}");
        NamedEventFired?.Invoke(eventName);
    }
}
