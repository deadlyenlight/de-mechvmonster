﻿using System.Collections.Generic;
using UnityEngine;

public abstract class MapRule : ScriptableObject
{
    public List<TerrainCell> BorderOptions = default;

    public class MapRuleData
    {
        public List<Vector3Int> borderEntities;
        public WorldGen.WorldMap objectMap;
        public Grid mapGrid;
        public int Height;
        public int Width;
    }

    public enum RepeatableMapRuleOption
    {
        NoRepeat,
        RepeatNumber,
        FillRemainingArea
    }

    public bool Handled { get; protected set; }

    public abstract List<Vector3Int> Undo(Dictionary<Vector3Int, GameObject> objectMap);
    public abstract List<Vector3Int> HandleRule(MapRuleData context);

    protected void UpdateBorder(MapRuleData context, List<Vector3Int> newBorderChunks)
    {
        // If this gets called by a rule, that rule should return 'no new border chunks' to the WorldGen script.
        if (context.borderEntities.Count == 0) { context.borderEntities = newBorderChunks; return; }

        var pointsToRemove = new List<Vector3Int>();
        foreach (var chunk in context.borderEntities)
        {
            if (context.objectMap.ContainsKey(chunk))
            {
                pointsToRemove.Add(chunk);
            }
        }

        foreach (var chunk in pointsToRemove)
        {
            context.borderEntities.Remove(chunk);
        }

        context.borderEntities.AddRange(newBorderChunks);
    }

    protected bool MapChunk(MapRuleData context, Vector3Int point, TerrainCell prefab)
    {
        bool mapSuccess = true;
        if (Mathf.Abs(point.x) > context.Width / 2 || Mathf.Abs(point.z) > context.Height / 2)
        {
            mapSuccess = false;
            prefab = BorderOptions[Random.Range(0, BorderOptions.Count)];
        }

        context.objectMap.Add(point,
            Instantiate(prefab, context.mapGrid.CellToWorld(point), prefab.transform.rotation));

        return mapSuccess;
    }
}
