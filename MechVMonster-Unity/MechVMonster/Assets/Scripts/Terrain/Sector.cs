﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

public class Sector
{
    private Dictionary<Vector3Int, TerrainCell> _sectorCells;
    private List<Vector3Int> _spawnerCells;
    private List<Vector3Int> _evacZoneCells;
    private MaterialPropertyBlock _sectorMaterial;

    public IReadOnlyDictionary<Vector3Int, TerrainCell> SectorCells => new ReadOnlyDictionary<Vector3Int, TerrainCell>(_sectorCells);

    public Sector(MaterialPropertyBlock material)
    {
        _sectorMaterial = material;

        _sectorCells = new Dictionary<Vector3Int, TerrainCell>();
        _spawnerCells = new List<Vector3Int>();
        _evacZoneCells = new List<Vector3Int>();
    }

    public bool AddCell(Vector3Int cellId, TerrainCell cell)
    {
        if (_sectorCells.ContainsKey(cellId)) return false;
        cell.TerrainRenderer.SetPropertyBlock(_sectorMaterial);
        _sectorCells.Add(cellId, cell);
        return true;
    }
}
