﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A class responsible for giving a terrain cell data it can work with for future map sectioning.
/// </summary>
public class TerrainCell : MonoBehaviour
{

    public List<InstanceSpawner> AllSpawners = default;
    public List<EvacZone> AllEvacZones = default;
    public Renderer TerrainRenderer = default;

    public bool HasSpawners => AllSpawners != null && AllSpawners.Count > 0;
}
