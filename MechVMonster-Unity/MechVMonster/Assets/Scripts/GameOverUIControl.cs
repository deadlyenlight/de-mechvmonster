﻿using System;
using UnityEngine;

public class GameOverUIControl : MonoBehaviour
{
    public event Action Restart;
    public event Action<int> BackToTitle;

    public void RestartClicked()
    {
        Restart?.Invoke();
    }

    public void BackToTitleClicked(int sceneIndex)
    {
        BackToTitle?.Invoke(sceneIndex);
    }
}
