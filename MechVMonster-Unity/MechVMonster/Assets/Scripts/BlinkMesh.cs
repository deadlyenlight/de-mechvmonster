﻿using System.Collections.Generic;
using UnityEngine;

public class BlinkMesh : MonoBehaviour
{
    [SerializeField] private Player _blinkable = default;
    [SerializeField] private List<Renderer> _targetRenderables = default;
    [SerializeField] private float _periodModifier = default;

    private List<Material> managedMaterials;
    private float timeSinceStart;
    private bool running;
    private bool visible;

    private void Start()
    {
        if (_blinkable != null)
        {
            _blinkable.PlayerDamaged += _blinkable_PlayerDamaged;
        }

        managedMaterials = new List<Material>();
        foreach (var renderer in _targetRenderables)
        {
            managedMaterials.Add(renderer.material);
        }
    }

    private void Update()
    {
        if (running)
        {
            timeSinceStart += Time.deltaTime;
            if (Mathf.Sin(timeSinceStart * _periodModifier) >= 0.0f && !visible)
            {
                ManageMaterial(1.0f);
            }
            else if (Mathf.Sin(timeSinceStart * _periodModifier) < 0.0f && visible)
            {
                ManageMaterial(0.0f);
            }
        }
    }

    private void OnDestroy()
    {
        foreach (var mat in managedMaterials)
        {
            Destroy(mat);
        }
        managedMaterials.Clear();
    }

    private void _blinkable_PlayerDamaged(bool obj)
    {
        if (obj)
        {
            timeSinceStart = 0.0f;
            running = true;
            visible = true;
        }
        else
        {
            running = false;
            visible = true;
            ManageMaterial(1.0f);
        }
    }

    private void ManageMaterial(float transparency)
    {
        foreach (var mat in managedMaterials)
        {
            Color matColor = mat.color;
            matColor.a = transparency;
            mat.color = matColor;
        }

        visible = transparency > 0.0f;
    }
}
