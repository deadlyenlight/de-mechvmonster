﻿using System;
using UnityEngine;
using UnityEngine.Events;

public interface ISpawnable
{
    string Name { get; set; }

    void InitializeSpawn(SpawnContext context);

    void RegisterThreshold(float threshold);
}

[Serializable]
public class SpawnContext
{
    [SerializeField] private Transform _playerTransform;
    [SerializeField] private Transform _evacZoneTransform;
    private SpawnAction.SpawnTarget _target;

    public Transform PlayerTransform { get => _playerTransform; }
    public Transform EvacZoneTransform { get => _evacZoneTransform; }

    public Transform Target { get => (_target == SpawnAction.SpawnTarget.Player) ? _playerTransform : _evacZoneTransform; }

    public SpawnContext(Transform player, Transform evac, SpawnAction.SpawnTarget target)
    {
        _playerTransform = player;
        _evacZoneTransform = evac;
        _target = target;
    }
}

[Serializable]
public class SpawnContextEvent : UnityEvent<SpawnContext> { }